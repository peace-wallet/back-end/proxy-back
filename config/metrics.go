package config

import "strings"

type MetricsConfig struct {
	Namespace string

	InterfaceConfig `yaml:",inline"`
}

func (c MetricsConfig) Validate(path string) (err error) {
	if c.Namespace == "" {
		return configurationError(path+".namespace", "namespace is required")
	}

	if strings.Contains(c.Namespace, "-") {
		return configurationError(path+".namespace", "namespace should not contain dashes")
	}

	return c.InterfaceConfig.Validate(path)
}
