package config

type FirebaseConfig struct {
	Credentials FirebaseCredentials `yaml:"credentials"`
}

// Validate checks correctness of the corresponding parameters.
// Returns error in case if critical configuration issue.
// Appends warning to the warnings array in case of potential misconfiguration found.
// `path` specifies the location of the configuration section in the config file.
func (c *FirebaseConfig) Validate(path string) (err error) {
	err = c.Credentials.Validate(path)
	return
}

type FirebaseCredentials struct {
	Path string `yaml:"path"`
}

// Validate checks correctness of the corresponding parameters.
// Returns error in case if critical configuration issue.
// Appends warning to the warnings array in case of potential misconfiguration found.
// `path` specifies the location of the configuration section in the config file.
func (c *FirebaseCredentials) Validate(path string) (err error) {
	if c.Path == "" {
		return configurationError(path+".path", "credentials file path could not be empty")
	}

	return
}
