package config

type CSTXSubscriptionProcessing struct {
	GetCSTXStateFromEADPeriodInSec      uint32 `yaml:"get-cstx-state-from-ead-period-in-sec"`
	DeliverCSTXStateToClientPeriodInSec uint32 `yaml:"deliver-cstx-state-to-client-period-in-sec"`
}
