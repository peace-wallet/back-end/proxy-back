package config

import (
	"testing"
)

func TestShardConfigValidationAvgBlocksCount(t *testing.T) {

	conf := func(avgBocks uint32) ShardConf {
		c := fixtureValidShardConf()
		c.AvgBlockTimeWindowSec = avgBocks
		return c
	}

	checkNegativeCases(t, []NegativeCase{
		{
			Conf:        conf(0),
			ExpectedErr: "tmp: average block time could not be set to 0 (configuration validation error)",
		},
	})

	checkWarnings(t, []NegativeCase{
		{
			Conf:        conf(1),
			ExpectedErr: "tmp: average block time is potentially misconfigured: specified — 1, safe — 10",
		},
		{
			Conf:        conf(5),
			ExpectedErr: "tmp: average block time is potentially misconfigured: specified — 5, safe — 10",
		},
		{
			Conf:        conf(9),
			ExpectedErr: "tmp: average block time is potentially misconfigured: specified — 9, safe — 10",
		},
	})
}

func TestBTCNetworkConfigNetworkName(t *testing.T) {
	conf := func(name string) *BTCNetworkConfig {
		c := fixtureValidBTCNetworkConfig()
		c.Name = name
		return &c
	}

	checkNegativeCases(t, []NegativeCase{
		{
			Conf: conf(""),
			ExpectedErr: "tmp: invalid network name set, allowed parameters are: " +
				"testnet, mainnet (configuration validation error)",
		},
		{
			Conf: conf("somenet"),
			ExpectedErr: "tmp: invalid network name set, allowed parameters are: " +
				"testnet, mainnet (configuration validation error)",
		},
	})

	checkPositiveCases(t, []PositiveCase{
		{
			Conf: conf("testnet"),
		},
		{
			Conf: conf("mainnet"),
		},
	})
}

func TestJAXNetworkConfigNetworkName(t *testing.T) {
	conf := func(name string) *JAXNetworkConfig {
		c := fixtureValidJaxNetworkConfig()
		c.Name = name
		return c
	}

	checkNegativeCases(t, []NegativeCase{
		{
			Conf: conf(""),
			ExpectedErr: "tmp: invalid network name set, allowed parameters are: " +
				"fastnet, testnet, mainnet (configuration validation error)",
		},
		{
			Conf: conf("somenet"),
			ExpectedErr: "tmp: invalid network name set, allowed parameters are: " +
				"fastnet, testnet, mainnet (configuration validation error)",
		},
	})

	checkPositiveCases(t, []PositiveCase{
		{
			Conf: conf("fastnet"),
		},
		{
			Conf: conf("testnet"),
		},
		{
			Conf: conf("mainnet"),
		},
	})
}

func TestJAXNetworkConfigEmptyShardsList(t *testing.T) {
	conf := func(shards []ShardConf) *JAXNetworkConfig {
		c := fixtureValidJaxNetworkConfig()
		c.Shards = shards
		return c
	}

	checkWarnings(t, []NegativeCase{
		{
			Conf:        conf([]ShardConf{}),
			ExpectedErr: "tmp.shards: there are no shards configuration specified",
		},
	})
}
