package config

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"sync"

	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
)

type IndexConfig struct {
	Disable bool   `yaml:"disable"`
	ID      uint32 `yaml:"id"`

	InterfaceConfig `yaml:",inline"`
}

// Validate checks correctness of the corresponding parameters.
// Returns error in case if critical configuration issue.
// Appends warning to the warnings array in case of potential misconfiguration found.
// `path` specifies the location of the configuration section in the config file.
func (c IndexConfig) Validate(path string) (err error) {
	if c.Disable {
		return nil
	}

	if strings.Contains(path, ".shards") {
		if c.ID == ec.BeaconShardIndex {
			report := fmt.Sprintf("shard could not have ID set to %d", ec.BeaconShardIndex)
			return configurationError(path+".id", report)

		} else if c.ID == ec.BTCShardIndex {
			report := fmt.Sprintf("shard could not have ID set to %d", ec.BTCShardIndex)
			return configurationError(path+".id", report)
		}

	} else if strings.Contains(path, ".btc") {
		if c.ID != ec.BTCShardIndex {
			report := fmt.Sprintf("BTC could not have ID different from %d", ec.BTCShardIndex)
			return configurationError(path+".id", report)
		}

	} else if strings.Contains(path, ".beacon") {
		if c.ID != ec.BeaconShardIndex {
			report := fmt.Sprintf("beacon could not have ID different from %d", ec.BeaconShardIndex)
			return configurationError(path+".id", report)
		}
	}

	err = c.InterfaceConfig.Validate(path)
	return
}

type IndexesSetConfig struct {
	BTC    IndexConfig   `yaml:"btc"`
	Beacon IndexConfig   `yaml:"beacon"`
	Shards []IndexConfig `yaml:"shards"`

	shardsConfigs sync.Map
}

// Validate checks correctness of the corresponding parameters.
// Returns error in case if critical configuration issue.
// Appends warning to the warnings array in case of potential misconfiguration found.
// `path` specifies the location of the configuration section in the config file.
func (i *IndexesSetConfig) Validate(path string) (err error) {
	err = i.BTC.Validate(path + ".btc")
	if err != nil {
		return
	}

	err = i.Beacon.Validate(path + ".beacon")
	if err != nil {
		return
	}

	if len(i.Shards) == 0 {
		appendWarning(path+".shards", "there are no shards configs specified")
	}
	for _, shard := range i.Shards {
		shardPath := fmt.Sprintf("%s.shards.%d", path, shard.ID)
		err = shard.Validate(shardPath)
		if err != nil {
			return
		}
	}

	return
}

func (i *IndexesSetConfig) InitShardsConfigs() {
	i.shardsConfigs.Store(i.Beacon.ID, i.Beacon)

	if !i.BTC.Disable {
		i.BTC.ID = ec.BTCShardIndex

		i.shardsConfigs.Store(ec.BTCShardIndex, i.BTC)
	}

	for _, shardConf := range i.Shards {
		i.shardsConfigs.Store(shardConf.ID, shardConf)
	}
}

func (i *IndexesSetConfig) GetShardConf(shardID uint32) (conf IndexConfig, err error) {
	c, ok := i.shardsConfigs.Load(shardID)
	if !ok {
		err = errors.New("no conf for shard " + strconv.Itoa(int(shardID)))
		return
	}

	conf, ok = c.(IndexConfig)
	if !ok {
		err = errors.New("invalid conf for shard")
		return
	}

	return
}

type IndexesConfig struct {
	UTXOs        IndexesSetConfig `yaml:"utxos"`
	Operations   IndexesSetConfig `yaml:"operations"`
	AddressesTxs IndexesSetConfig `yaml:"addresses-txs"`
}

// Validate checks correctness of the corresponding parameters.
// Returns error in case if critical configuration issue.
// Appends warning to the warnings array in case of potential misconfiguration found.
// `path` specifies the location of the configuration section in the config file.
func (c *IndexesConfig) Validate(path string) (err error) {
	err = c.UTXOs.Validate(path + ".utxos")
	if err != nil {
		return
	}

	err = c.UTXOs.Validate(path + ".operations")
	if err != nil {
		return
	}

	err = c.UTXOs.Validate(path + ".addresses-txs")
	if err != nil {
		return
	}

	return
}
