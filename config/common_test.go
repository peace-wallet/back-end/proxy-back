package config

import (
	"fmt"
	"testing"

	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
)

type Validator interface {
	Validate(path string) (err error)
}

type NegativeCase struct {
	Conf        Validator
	Path        string
	ExpectedErr string
}

type PositiveCase struct {
	Conf Validator
	Path string
}

func checkNegativeCases(t *testing.T, cases []NegativeCase) {
	for _, c := range cases {

		// Most of the test cases are path independent.
		// But there are some tes cases that require specific path to be set.
		// By default, for most part of test cases, Path is empty.
		if c.Path == "" {
			c.Path = "tmp"
		}

		err := c.Conf.Validate(c.Path)
		if err == nil {
			t.Error("No error in negative test case")
			continue
		}

		if err.Error() != c.ExpectedErr {
			report := fmt.Sprintf(
				"errors does not match: expected - `%s`, received - `%s`", c.ExpectedErr, err.Error())
			t.Error(report)
			continue
		}
	}
}

func checkPositiveCases(t *testing.T, cases []PositiveCase) {
	for _, c := range cases {
		if c.Path == "" {
			c.Path = "tmp"
		}

		err := c.Conf.Validate(c.Path)
		if err != nil {
			report := fmt.Sprintf("unexpected error occured: %s", err.Error())
			t.Error(report)
			continue
		}
	}
}

func checkWarnings(t *testing.T, cases []NegativeCase) {
	for _, c := range cases {
		// Reset warnings.
		warnings = warnings[:0]

		if c.Path == "" {
			c.Path = "tmp"
		}

		_ = c.Conf.Validate(c.Path)
		if len(warnings) == 0 {
			t.Error("No warning has been reported")
			continue
		}

		warning := warnings[0]
		if warning.Error() != c.ExpectedErr {
			report := fmt.Sprintf(
				"warnings does not match: expected - `%s`, received - `%s`", c.ExpectedErr, warning.Error())
			t.Error(report)
			continue
		}
	}
}

func fixtureValidIndexConfig() IndexConfig {
	return IndexConfig{
		ID:              0,
		InterfaceConfig: fixtureValidInterfaceConfig(),
	}
}

func fixtureValidIndexesSetConfig() *IndexesSetConfig {
	btc := fixtureValidIndexConfig()
	btc.ID = ec.BTCShardIndex

	beacon := fixtureValidIndexConfig()
	beacon.ID = ec.BeaconShardIndex

	return &IndexesSetConfig{
		BTC:    btc,
		Beacon: beacon,
		Shards: []IndexConfig{fixtureValidIndexConfig(), fixtureValidIndexConfig()},
	}
}

func fixtureValidInterfaceConfig() InterfaceConfig {
	return InterfaceConfig{
		Host: "127.0.0.1",
		Port: 3000,
	}
}

func fixtureValidCredentialsConfig() CredentialsConfig {
	return CredentialsConfig{
		User: "user",
		Pass: "pass",
	}
}

func fixtureValidShardConf() ShardConf {
	return ShardConf{
		ID:                    0,
		AvgBlockTimeWindowSec: 20,
		InterfaceConfig:       fixtureValidInterfaceConfig(),
		CredentialsConfig:     fixtureValidCredentialsConfig(),
	}
}

func fixtureValidBTCNetworkConfig() BTCNetworkConfig {
	return BTCNetworkConfig{
		Name: "mainnet",
	}
}

func fixtureValidJaxNetworkConfig() *JAXNetworkConfig {
	return &JAXNetworkConfig{
		Name:   "mainnet",
		Beacon: fixtureValidShardConf(),
		Shards: []ShardConf{fixtureValidShardConf(), fixtureValidShardConf()},
	}
}
