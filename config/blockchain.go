package config

import (
	"errors"
	"fmt"
	"os"
	"sync"

	btccfg "github.com/btcsuite/btcd/chaincfg"
	jaxcfg "gitlab.com/jaxnet/jaxnetd/types/chaincfg"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
)

type ShardConf struct {
	ID                    uint32 `yaml:"id"`
	AvgBlockTimeWindowSec uint32 `yaml:"avg-block-time-seconds"`

	InterfaceConfig   `yaml:",inline"`
	CredentialsConfig `yaml:",inline"`
}

// Validate checks correctness of the corresponding parameters.
// Returns error in case if critical configuration issue.
// Appends warning to the warnings array in case of potential misconfiguration found.
// `path` specifies the location of the configuration section in the config file.
func (c ShardConf) Validate(path string) (err error) {
	if c.AvgBlockTimeWindowSec == 0 {
		return configurationError(path, "average block time could not be set to 0")
	}

	const minimalSecondsSafe = 10
	if c.AvgBlockTimeWindowSec < minimalSecondsSafe {
		warn := fmt.Sprintf(
			"average block time is potentially misconfigured: specified — %d, safe — %d",
			c.AvgBlockTimeWindowSec, minimalSecondsSafe)

		appendWarning(path, warn)
	}

	err = c.InterfaceConfig.Validate(path)
	if err != nil {
		return
	}

	return
}

const (
	btcTestNetName = "testnet3"
	btcMainNetName = "mainnet"
)

type BTCNetworkConfig struct {
	Disable bool   `yaml:"disable"`
	Name    string `yaml:"name"`
	ID      uint32 `yaml:"-"` // Would be init in runtime by a constant value.
	APIKey  string `yaml:"-"` // Should be parsed from secrets, not from config!
}

func (c *BTCNetworkConfig) NetworkCfg() (config *btccfg.Params, err error) {
	switch c.Name {
	case btcTestNetName:
		config = &btccfg.TestNet3Params

	case btcMainNetName:
		config = &btccfg.MainNetParams

	default:
		err = errors.New("invalid network name")
	}

	return
}

// Validate checks correctness of the corresponding parameters.
// Returns error in case if critical configuration issue.
// Appends warning to the warnings array in case of potential misconfiguration found.
// `path` specifies the location of the configuration section in the config file.
func (c *BTCNetworkConfig) Validate(path string) (err error) {
	if c.Disable {
		return nil
	}

	if c.Name != btcTestNetName && c.Name != btcMainNetName {
		report := fmt.Sprintf("invalid network name set, allowed parameters are: %s, %s", btcTestNetName, btcMainNetName)
		return configurationError(path, report)
	}

	err = c.loadSecrets()
	if err != nil {
		panic("Could not read BTC API secret API Key")
		return
	}

	return
}

func (c *BTCNetworkConfig) loadSecrets() (err error) {
	key, err := os.ReadFile("./secrets/btc_api.key")
	if err != nil {
		panic("No BTC API key provided")
	}

	c.APIKey = string(key)
	if c.APIKey == "" {
		panic("No BTC API key provided")
	}

	return
}

const (
	jaxFastNetName = "fastnet"
	jaxTestNetName = "testnet"
	jaxMainNetName = "mainnet"
)

type JAXNetworkConfig struct {
	Name   string      `yaml:"name"`
	Beacon ShardConf   `yaml:"beacon"`
	Shards []ShardConf `yaml:"shards"`

	shardsConfigs sync.Map
}

func (jc *JAXNetworkConfig) NetworkCfg() (config *jaxcfg.Params, err error) {
	switch jc.Name {
	case "testnet":
		config = &jaxcfg.TestNet3Params

	case "fastnet":
		config = &jaxcfg.FastNetParams

	case "mainnet":
		config = &jaxcfg.MainNetParams

	default:
		err = errors.New("invalid network name")
	}

	return
}

// Validate checks correctness of the corresponding parameters.
// Returns error in case if critical configuration issue.
// Appends warning to the warnings array in case of potential misconfiguration found.
// `path` specifies the location of the configuration section in the config file.
func (jc *JAXNetworkConfig) Validate(path string) (err error) {
	if jc.Name != jaxFastNetName && jc.Name != jaxTestNetName && jc.Name != jaxMainNetName {
		report := fmt.Sprintf("invalid network name set, allowed parameters are: %s, %s, %s", jaxFastNetName, jaxTestNetName, jaxMainNetName)
		return configurationError(path, report)
	}

	err = jc.Beacon.Validate(path + ".beacon")
	if err != nil {
		return
	}

	if len(jc.Shards) == 0 {
		appendWarning(path+".shards", "there are no shards configuration specified")
	} else {
		for _, shard := range jc.Shards {
			err = shard.Validate(fmt.Sprintf(path, ".shards.", shard.ID))
			if err != nil {
				return
			}
		}
	}

	return
}

func (jc *JAXNetworkConfig) InitShardsConfigs() {
	jc.shardsConfigs.Store(jc.Beacon.ID, jc.Beacon)
	for _, shardConf := range jc.Shards {
		jc.shardsConfigs.Store(shardConf.ID, shardConf)
	}
}

func (jc *JAXNetworkConfig) Config(shardID uint32) (config ShardConf, err error) {
	c, ok := jc.shardsConfigs.Load(shardID)
	if !ok {
		err = errors.New("no conf for shard")
		return
	}

	config, ok = c.(ShardConf)
	if !ok {
		err = errors.New("invalid conf for shard")
		return
	}

	return
}

type BlockchainConfig struct {
	JAX JAXNetworkConfig `yaml:"jax"`
	BTC BTCNetworkConfig `yaml:"btc"`
}

// Validate checks correctness of the corresponding parameters.
// Returns error in case if critical configuration issue.
// Appends warning to the warnings array in case of potential misconfiguration found.
// `path` specifies the location of the configuration section in the config file.
func (bc *BlockchainConfig) Validate(path string) (err error) {
	err = bc.JAX.Validate(path + ".jax")
	if err != nil {
		return
	}

	err = bc.BTC.Validate(path + ".btc")
	if err != nil {
		return
	}
	return
}

func (bc *BlockchainConfig) PreprocessConfigs() {
	bc.BTC.ID = ec.BTCShardIndex

	bc.JAX.InitShardsConfigs()
}
