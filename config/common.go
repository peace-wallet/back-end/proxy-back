package config

import (
	"fmt"

	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
)

func configurationError(path, message string) (err error) {
	return fmt.Errorf("%s: %s (%w)", path, message, ec.ErrConfigValidation)
}

func appendWarning(path, message string) {
	warning := fmt.Errorf("%s: %s", path, message)
	warnings = append(warnings, warning)
	return
}
