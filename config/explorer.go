package config

// ExplorerConfig defines the connection credentials for the Jax.Explorer.
// Explorer is used for fetching some publicly available info, like shards summary, addresses balances, etc.
type ExplorerConfig struct {
	InterfaceConfig `yaml:",inline"`
}

// Validate checks correctness of the corresponding parameters.
// Returns error in case if critical configuration issue.
// Appends warning to the warnings array in case of potential misconfiguration found.
// `path` specifies the location of the configuration section in the config file.
func (c ExplorerConfig) Validate(path string) (err error) {
	err = c.InterfaceConfig.Validate(path)
	return
}
