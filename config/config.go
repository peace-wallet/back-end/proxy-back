package config

import (
	"os"
	"strings"

	"gitlab.com/peace-wallet/back-end/proxy-back/logger"
	"gopkg.in/yaml.v3"
)

var (
	Config = &Settings{}

	// Globally accessible list of warnings, that are collected during config file validation.
	// It is assumed that config is processed synchronously, so no concurrent access is assumed to this array.
	warnings = make([]error, 0, 16)
)

type Settings struct {
	InterfaceConfig `yaml:",inline"`

	Verbosity      string                     `yaml:"verbosity"`
	Explorer       ExplorerConfig             `yaml:"explorer"`
	Blockchain     BlockchainConfig           `yaml:"blockchain"`
	Metrics        *MetricsConfig             `yaml:"metrics,omitempty"`
	Firebase       FirebaseConfig             `yaml:"firebase"`
	Database       DatabaseConfig             `yaml:"database"`
	CSTXProcessing CSTXSubscriptionProcessing `yaml:"cstx-subscription-processing"`
}

func Load(path string) (err error) {
	file, err := os.OpenFile(path, os.O_RDONLY, 0644)
	if err != nil {
		return
	}

	err = yaml.NewDecoder(file).Decode(&Config)
	if err != nil {
		return
	}

	// Configs preprocessing + defaults values setting.
	Config.Verbosity = strings.ToUpper(Config.Verbosity)
	Config.Blockchain.PreprocessConfigs()

	err = validate()
	if err != nil {
		return
	}

	// Warnings processing (if any).
	for _, warning := range warnings {
		logger.Log.Warn().Str("ctx", "settings").Msg(warning.Error())
	}
	return
}

func validate() (err error) {
	if Config.Verbosity != "DEBUG" && Config.Verbosity != "INFO" && Config.Verbosity != "TRACE" {
		return configurationError("verbosity", "invalid verbosity level set, available: TRACE, DEBUG, INFO")
	}

	err = Config.Explorer.Validate("explorer")
	if err != nil {
		return
	}

	err = Config.Blockchain.Validate("blockchain")
	if err != nil {
		return
	}

	if Config.Metrics != nil {
		err = Config.Metrics.Validate("metrics")
		if err != nil {
			return
		}
	}

	err = Config.Firebase.Validate("firebase")
	if err != nil {
		return
	}

	// todo: add Database config validation.
	// 		 https://app.asana.com/0/1200658259238392/1200755906894477/f

	// todo: add CSTXSubscriptionProcessing config validation.
	// 		 https://app.asana.com/0/1200658259238392/1200755906894477/f

	return
}

func TraceEnabled() bool {
	return Config.Verbosity == "TRACE"
}
