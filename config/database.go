package config

import "fmt"

type DatabaseConfig struct {
	Net            InterfaceConfig   `yaml:",inline"`
	Credentials    CredentialsConfig `yaml:",inline"`
	DBName         string            `yaml:"db-name"`
	ValidateSchema bool              `yaml:"validate-schema"`
}

func (c DatabaseConfig) PSQLConnectionCredentials() string {
	return fmt.Sprint("postgres://", c.Credentials.User, ":", c.Credentials.Pass, "@", c.Net.Host, ":", c.Net.Port, "/"+c.DBName)
}
