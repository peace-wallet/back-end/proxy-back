package config

import "fmt"

type InterfaceConfig struct {
	Host string `yaml:"host"`
	Port uint16 `yaml:"port"`
}

func (i InterfaceConfig) Interface() string {
	return fmt.Sprint(i.Host, ":", i.Port)
}

func (i InterfaceConfig) HostURL() string {
	return fmt.Sprintf("http://%v:%v", i.Host, i.Port)
}

func (i InterfaceConfig) Validate(path string) (err error) {
	if i.Host == "" {
		return configurationError(path, "host could not empty")
	}

	if i.Port == 0 {
		return configurationError(path, "port could not be set to 0")
	}

	return
}

type CredentialsConfig struct {
	User string `yaml:"user"`
	Pass string `yaml:"pass"`
}
