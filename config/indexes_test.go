package config

import (
	"testing"

	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
)

func TestIndexConfigShardID(t *testing.T) {
	conf := func(id uint32) IndexConfig {
		c := fixtureValidIndexConfig()
		c.ID = id
		return c
	}

	checkNegativeCases(t, []NegativeCase{
		{
			Conf:        conf(0),
			Path:        "indexes.shards", // required for test to be executed properly
			ExpectedErr: "indexes.shards.id: shard could not have ID set to 0 (configuration validation error)",
		},
	})

	checkPositiveCases(t, []PositiveCase{
		{
			Conf: conf(1),
		},
		{
			Conf: conf(2),
		},
		{
			Conf: conf(10),
		},
	})
}

func TestIndexesSetConfigBtcID(t *testing.T) {
	conf := func(id uint32) *IndexesSetConfig {
		c := fixtureValidIndexesSetConfig()
		c.BTC.ID = id
		c.Shards = nil
		return c
	}

	checkNegativeCases(t, []NegativeCase{
		{
			Conf:        conf(0),
			ExpectedErr: "indexes.btc.id: BTC could not have ID different from 4294967295 (configuration validation error)",
			Path:        "indexes",
		},
		{
			Conf:        conf(1),
			ExpectedErr: "indexes.btc.id: BTC could not have ID different from 4294967295 (configuration validation error)",
			Path:        "indexes",
		},
	})

	checkPositiveCases(t, []PositiveCase{
		{
			Conf: conf(ec.BTCShardIndex),
			Path: "indexes",
		},
	})
}

func TestIndexesSetConfigBeaconID(t *testing.T) {
	conf := func(id uint32) *IndexesSetConfig {
		c := fixtureValidIndexesSetConfig()
		c.Beacon.ID = id
		c.Shards = nil
		return c
	}

	checkNegativeCases(t, []NegativeCase{
		{
			Conf:        conf(1),
			ExpectedErr: "tmp.beacon.id: beacon could not have ID different from 0 (configuration validation error)",
		},
		{
			Conf:        conf(ec.BTCShardIndex),
			ExpectedErr: "tmp.beacon.id: beacon could not have ID different from 0 (configuration validation error)",
		},
	})

	checkPositiveCases(t, []PositiveCase{
		{
			Conf: conf(0),
		},
	})
}

func TestIndexesSetConfigShardID(t *testing.T) {
	conf := func(id uint32) *IndexesSetConfig {
		shard := fixtureValidIndexConfig()
		shard.ID = id

		c := fixtureValidIndexesSetConfig()
		c.Shards = []IndexConfig{shard}
		return c
	}

	checkNegativeCases(t, []NegativeCase{
		{
			Conf:        conf(0),
			ExpectedErr: "indexes.shards.0.id: shard could not have ID set to 0 (configuration validation error)",
			Path:        "indexes",
		},
		{
			Conf:        conf(ec.BTCShardIndex),
			ExpectedErr: "indexes.shards.4294967295.id: shard could not have ID set to 4294967295 (configuration validation error)",
			Path:        "indexes",
		},
	})

	checkPositiveCases(t, []PositiveCase{
		{
			Conf: conf(1),
			Path: "indexes",
		},
		{
			Conf: conf(1),
			Path: "indexes",
		},
	})
}

func TestIndexesSetConfigWarnings(t *testing.T) {
	conf := func(id uint32) *IndexesSetConfig {
		c := fixtureValidIndexesSetConfig()
		c.Shards = nil
		return c
	}

	checkWarnings(t, []NegativeCase{
		{
			Conf:        conf(0),
			ExpectedErr: "indexes.shards: there are no shards configs specified",
			Path:        "indexes",
		},
	})
}
