package main

import (
	"flag"
	"fmt"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"runtime/debug"
	"syscall"
	"time"

	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
	"gitlab.com/peace-wallet/back-end/proxy-back/api"
	"gitlab.com/peace-wallet/back-end/proxy-back/config"
	"gitlab.com/peace-wallet/back-end/proxy-back/database"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
	"gitlab.com/peace-wallet/back-end/proxy-back/logger"
	"gitlab.com/peace-wallet/back-end/proxy-back/metrics"
	"gitlab.com/peace-wallet/back-end/proxy-back/utils/fees"
)

func main() {
	// pprof
	// todo: delete me after profiling is ended
	go func() {
		_ = http.ListenAndServe("localhost:6000", nil)
	}()

	var (
		err          error
		processError = func(err *error) {
			if (*err) != nil {
				if logger.Log != nil {
					logger.Log.Err(*err).Msg("critical error occurred")

				} else {
					// No logger is initialized, printing to the stderr.
					fmt.Fprint(os.Stderr, (*err).Error())
				}
				os.Exit(-1)
			}
		}
	)
	defer processError(&err)

	configFilePath := flag.String("config", "config.yaml", "path to config file")
	flag.Parse()

	logger.Init()
	err = config.Load(*configFilePath)
	if err != nil {
		return
	}

	metrics.Init()
	time.Sleep(time.Second * 3)
	err = InitMetrics()
	if err != nil {
		return
	}

	err = database.InitConnectionsPool()
	if err != nil {
		return
	}

	err = database.EnsureSchema()
	if err != nil {
		return
	}

	r := router.New()

	r.GET("/api/v1/addresses/{address}/balances/", MetricsMiddleware(api.AddressBalances))
	r.GET("/api/v1/addresses/{address}/history/", MetricsMiddleware(api.AddressHistory))
	r.GET("/api/v1/shards/{shard-id}/addresses/{address}/utxos/", MetricsMiddleware(api.AddressUTXOs))
	r.GET("/api/v1/shards/{shard-id}/transactions/", MetricsMiddleware(api.Transactions))
	r.POST("/api/v1/shards/{shard-id}/transactions/", MetricsMiddleware(api.TransactionsRaw))
	r.POST("/api/v1/shards/{shard-id}/transactions/publish/", MetricsMiddleware(api.TransactionPublishing))
	r.GET("/api/v1/shards/{shard-id}/addresses/{address}/check-input-tx/", MetricsMiddleware(api.CheckInputTx))
	r.GET("/api/v1/exchange-agents/", MetricsMiddleware(api.ExchangeAgents))
	r.POST("/api/v1/cross-shard-tx/subscribe/", MetricsMiddleware(api.CrossShardTxSubscribe))
	r.GET("/api/v1/addresses/{address}/locked-utxos/", MetricsMiddleware(api.AddressLockedUTXOs))

	r.GET("/api/v2/addresses/{address}/balances/", MetricsMiddleware(api.AddressBalances)) // todo: update
	r.GET("/api/v2/addresses/{address}/history/", MetricsMiddleware(api.V2AddressHistory))
	r.GET("/api/v2/shards/{shard-id}/addresses/{address}/utxos/", MetricsMiddleware(api.V2AddressUTXOs))
	r.GET("/api/v2/shards/{shard-id}/transactions/", MetricsMiddleware(api.V2TransactionsRaw))
	r.GET("/api/v2/shards/{shard-id}/transactions/{hash}/details/", MetricsMiddleware(api.V2TransactionDetails))
	r.POST("/api/v2/shards/{shard-id}/transactions/publish/", MetricsMiddleware(api.V2TransactionPublish))

	server := fasthttp.Server{
		Handler: r.Handler,
		Logger:  logger.Log, // todo: suppress those bloated hex errors

		// In production env we have a lot of unnecessary and/or malicious request from strangers.
		// Those requests are broken or invalid and server can't process them. This is ok.
		// This flag will suppress errors such as
		// 'connection reset by peer', 'broken pipe' and 'connection timeout' also.
		LogAllErrors:          false,
		SecureErrorLogMessage: true,
	}

	if err := fees.LoadActualFees(); err != nil {
		processError(&err)
		return
	}

	go func() {
		logger.Log.Info().Msg("Staring server @ " + config.Config.Interface())
		if err := server.ListenAndServe(config.Config.Interface()); err != nil {
			processError(&err)
			return
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 10 seconds.
	// Use a buffered channel to avoid missing signals as recommended for signal.Notify
	var interruptSignals = []os.Signal{syscall.SIGINT, syscall.SIGTERM}
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, interruptSignals...)
	<-quit

	logger.Log.Info().Msg("Stop signal received. Stopping")
	os.Exit(0)
}

func InitMetrics() (err error) {
	var (
		blocksProcessingHistogramBuckets = []float64{0.25, 0.5, 0.75, 1, 1.5, 2, 5, 10, 20, 50, 100}
	)

	err = metrics.InitGauge(ec.MetricHTTPRequest, ec.MetricLastAction, "")
	if err != nil {
		return
	}

	err = metrics.InitHistogram(ec.MetricHTTPRequest, ec.MetricProcessingTime, "", blocksProcessingHistogramBuckets)
	if err != nil {
		return
	}

	//
	// Other metrics goes here.
	//
	return
}

func MetricsMiddleware(handler fasthttp.RequestHandler) fasthttp.RequestHandler {
	middleware := func(h fasthttp.RequestHandler) fasthttp.RequestHandler {
		return func(ctx *fasthttp.RequestCtx) {
			log := logger.Log.With().
				Str("path", string(ctx.Path())).
				Str("method", string(ctx.Method())).
				Str("user_agent", string(ctx.UserAgent())).
				Str("user_agent", string(ctx.UserAgent())).
				Str("remote_address", ctx.RemoteAddr().String()).
				Logger()
			processingStartTime := time.Now()

			defer func() {
				if rvr := recover(); rvr != nil && rvr != http.ErrAbortHandler {
					processingEndTime := time.Now()
					timeDelta := processingEndTime.Sub(processingStartTime)

					log.Error().
						Stringer("duration", timeDelta).
						Interface("error", rvr).
						Msg("Request failed with panic")
					debug.PrintStack()

					ctx.Error("internal server error", 500)
				}
			}()

			h(ctx)

			processingEndTime := time.Now()
			timeDelta := processingEndTime.Sub(processingStartTime)
			metrics.GaugeSet(ec.MetricLastAction, float64(processingEndTime.Unix()))
			metrics.HistogramObserve(ec.MetricProcessingTime, timeDelta.Seconds())

			log.Debug().
				Stringer("duration", timeDelta).
				Int("status", ctx.Response.StatusCode()).
				Msg("Request finished")
		}
	}
	return middleware(handler)
}
