package firebase

import (
	"context"
	"fmt"

	"firebase.google.com/go/messaging"

	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
)

var (
	app    *firebase.App
	client *messaging.Client
)

func Init() (err error) {
	opt := option.WithCredentialsFile("secrets/firebase-account.json")
	app, err = firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		return fmt.Errorf("can't initalize firebase app: %w", err)
	}

	client, err = app.Messaging(context.Background())
	return
}

func NotifyTXIsReady(txUUID, token string) (err error) {
	var (
		title = "Cross Shard TX is ready"
		body  = "Cross Shard TX could now be fetched and signed"
		data  = map[string]string{
			"uuid":   txUUID,
			"status": "0",
		}
	)

	return sendPush(title, body, token, data)
}

func NotifyTXFailed(txUUID, token string) (err error) {
	var (
		title = "EAD failed to execute Cross Shard TX"
		body  = "Cross Shard TX could not be finished. Please, execute refund TX to get locked money back."
		data  = map[string]string{
			"uuid":   txUUID,
			"status": "255",
		}
	)

	return sendPush(title, body, token, data)
}

func sendPush(title, body, token string, data map[string]string) (err error) {
	var iOSData = make(map[string]interface{})
	for key, value := range data {
		iOSData[key] = value
	}

	msg := &messaging.MulticastMessage{
		Tokens: []string{token},
		Data:   data,
		Notification: &messaging.Notification{
			Title: title,
			Body:  body,
			//ImageURL: "",
		},
		Android: &messaging.AndroidConfig{
			//CollapseKey:           "",
			//Priority:              "",
			//TTL:                   nil,
			//RestrictedPackageName: "",
			Data: data,
			Notification: &messaging.AndroidNotification{
				Title: title,
				Body:  body,
				//Icon:                  "",
				//Color:                 "",
				//Sound:                 "",
				//Tag:                   "",
				//ClickAction:           "",
				//BodyLocKey:            "",
				//BodyLocArgs:           nil,
				//TitleLocKey:           "",
				//TitleLocArgs:          nil,
				//ChannelID:             "",
				//ImageURL:              "",
				//Ticker:                "",
				//Sticky:                false,
				//EventTimestamp:        nil,
				//LocalOnly:             false,
				//Priority:              0,
				//VibrateTimingMillis:   nil,
				//DefaultVibrateTimings: false,
				//DefaultSound:          false,
				//LightSettings:         nil,
				//DefaultLightSettings:  false,
				//Visibility:            0,
				//NotificationCount:     nil,
			},
			//FCMOptions:            nil,
		},
		Webpush: &messaging.WebpushConfig{
			//Headers:      nil,
			Data: data,
			Notification: &messaging.WebpushNotification{
				//Actions:            nil,
				Title: title,
				Body:  body,
				//Icon:               "",
				//Badge:              "",
				//Direction:          "",
				Data: data,
				//Image:              "",
				//Language:           "",
				//Renotify:           false,
				//RequireInteraction: false,
				//Silent:             false,
				//Tag:                "",
				//TimestampMillis:    nil,
				//Vibrate:            nil,
				//CustomData:         nil,
			},
			//FcmOptions:   nil,
		},
		APNS: &messaging.APNSConfig{
			//Headers:    nil,
			Payload: &messaging.APNSPayload{
				Aps: &messaging.Aps{
					//AlertString:      "",
					//Alert:            nil,
					//Badge:            nil,
					//Sound:            "",
					//CriticalSound:    nil,
					//ContentAvailable: false,
					//MutableContent:   false,
					//Category:         "",
					//ThreadID:         "",
					CustomData: iOSData,
				},
				CustomData: iOSData,
			},
			//FCMOptions: nil,
		},
	}

	_, err = client.SendMulticast(context.Background(), msg)
	return
}
