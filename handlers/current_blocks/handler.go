package current_blocks

import (
	"fmt"
	"sync"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/peace-wallet/back-end/proxy-back/config"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
	"gitlab.com/peace-wallet/back-end/proxy-back/logger"
	"gitlab.com/peace-wallet/back-end/proxy-back/rpc"
)

const (
	logCtx = "current-blocks-handler"
)

var (
// CurrentBlockHandler = &CurrentBlock{}
)

// Deprecated
type CurrentBlock struct {
	blocks sync.Map
}

func (h *CurrentBlock) Run() {
	preventNetworkResourcesSpike := func() {
		time.Sleep(time.Millisecond * 150)
	}

	// btcRoundDelay := time.Duration(config.Config.Blockchain.BTC.AvgBlockTimeWindowSec) * time.Second
	// if config.TraceEnabled() {
	// 	trace().Dur(
	// 		"delay", btcRoundDelay).Msg(
	// 		"Next block fetching round delay for BTC set")
	// }

	beaconRoundDelay := time.Duration(config.Config.Blockchain.JAX.Beacon.AvgBlockTimeWindowSec) * time.Second
	if config.TraceEnabled() {
		trace().Dur(
			"delay", beaconRoundDelay).Msg(
			"Next block fetching round delay for beacon set")
	}
	go h.fetchBlocksRegularly(
		func(shardID uint32) error {
			return h.fetchAndUpdateJaxBlock(shardID)
		}, beaconRoundDelay, ec.BeaconShardIndex)
	preventNetworkResourcesSpike()

	for _, shard := range config.Config.Blockchain.JAX.Shards {
		shardCfg, err := config.Config.Blockchain.JAX.Config(shard.ID)
		if err != nil {
			return
		}

		roundDelay := time.Duration(shardCfg.AvgBlockTimeWindowSec) * time.Second
		if config.TraceEnabled() {
			trace().Uint32(
				"shard-id", shardCfg.ID).Dur(
				"delay", roundDelay).Msg(
				"Next block fetching round delay for shard set")
		}

		go h.fetchBlocksRegularly(
			func(shardID uint32) error {
				return h.fetchAndUpdateJaxBlock(shardID)
			}, roundDelay, shardCfg.ID)

		// WARN: This code should follow AFTER the h.fetchBlocksRegularly call.
		// Otherwise, there is a probability that shardID variable would change too fast,
		// and two goroutines would  be launched with the same shardID.
		preventNetworkResourcesSpike()
	}
}

func (h *CurrentBlock) BlockCount(shardID uint32) (count int64) {
	c, ok := h.blocks.Load(shardID)
	if !ok {
		return
	}

	count = c.(int64)
	return
}

func (h *CurrentBlock) fetchBlocksRegularly(handler func(shardID uint32) error,
	normalRoundDelay time.Duration, shardID uint32) {

	var (
		err              error
		failedRoundDelay = normalRoundDelay * 4
	)

	for {
		err = handler(shardID)
		if err != nil {
			logger.Log.Err(err).Msg(
				"can't fetch current block -> round failed, waiting extra time for the node to be fixed")

			time.Sleep(failedRoundDelay)

		} else {
			time.Sleep(normalRoundDelay)
		}
	}
}

// func (h *CurrentBlock) fetchAndUpdateBTCBlock(_ uint32) (err error) {
// 	var (
// 		client      *btcrpc.Client
// 		blocksCount int64
// 	)
//
// 	defer func() {
// 		if client != nil {
// 			rpc.ReleaseBTCClient(client)
// 		}
// 	}()
//
// 	client, err = rpc.AcquireBTCClient()
// 	if err != nil {
// 		return fmt.Errorf("can't aquire BTC client: %w", err)
// 	}
//
// 	blocksCount, err = client.GetBlockCount()
// 	if err != nil {
// 		return fmt.Errorf("can't fetch BTC block: %w", err)
// 	}
//
// 	if config.TraceEnabled() {
// 		trace().Uint32(
// 			"shard-id", ec.BTCShardIndex).Int64(
// 			"block-height", blocksCount).Msg(
// 			"Set block height")
// 	}
// 	h.blocks.Store(config.Config.Blockchain.BTC.ID, blocksCount)
// 	return
// }

func (h *CurrentBlock) fetchAndUpdateJaxBlock(shardID uint32) (err error) {
	var (
		blocksCount int64
	)

	client, err := rpc.AcquireJAXClient(shardID)
	if err != nil {
		return fmt.Errorf("can't aquire jax client (shard %d): %w", shardID, err)
	}

	blocksCount, err = client.GetBlockCount(shardID)
	if err != nil {
		return fmt.Errorf("can't fetch next block (shard %d): %w", shardID, err)
	}

	h.blocks.Store(shardID, blocksCount)
	if config.TraceEnabled() {
		trace().Uint32(
			"shard-id", shardID).Int64(
			"block-height", blocksCount).Msg(
			"Set block height")
	}

	return
}

func trace() *zerolog.Event {
	return logger.Log.Trace().Str("ctx", logCtx)
}

func logError(err error) *zerolog.Event {
	return logger.Log.Err(err).Str("ctx", logCtx)
}
