package current_blocks

import (
	"fmt"
	"sync"
	"time"

	"github.com/valyala/fasthttp"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/btcapi"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
)

var (
	currentBTCBlock int64
	lastUpdated     time.Time
	mutex           sync.Mutex
)

// GetLastBtcBlock returns last block height of the BTC network.
// Fetches the BTC API for the info and caches it for 10 minutes.
// During this period returns cached value.
func GetLastBtcBlock() (height int64, err error) {
	mutex.Lock()
	defer mutex.Unlock()

	if currentBTCBlock == 0 || lastUpdated.Before(time.Now().Add(time.Minute*10*-1)) {
		currentBTCBlock, err = fetchBlockHeight()
		if err != nil {
			return
		}

		lastUpdated = time.Now()
	}

	height = currentBTCBlock
	return
}

func fetchBlockHeight() (height int64, err error) {
	var (
		blockRaw   interface{}
		statusCode int
	)
	blockRaw, statusCode, err = btcapi.GetOneItem("blockchain-data", "blocks/last")
	if err != nil {
		err = fmt.Errorf("BTC API call failed: %w", err)
		return
	}

	if statusCode != fasthttp.StatusOK {
		err = fmt.Errorf("BTC API call failed with response: %+v", blockRaw)
		return
	}

	blockData, ok := blockRaw.(map[string]interface{})
	if !ok {
		err = ec.EmitError("cant convert API response: %+v", blockRaw)
		return
	}

	heightRaw, heightIsPresent := blockData["height"]
	if !heightIsPresent {
		err = ec.EmitError("field `height` is missed in response: %+v", blockData)
		return
	}

	heightFloat, converted := heightRaw.(float64)
	if !converted {
		err = ec.EmitError("cant parse field `height` from the response: %+v", blockData)
		return
	}

	height = int64(heightFloat)
	return
}
