# Compile stage
FROM golang:alpine AS build-env
RUN apk add --no-cache git bash

#ENV GOPROXY=direct
ENV GO111MODULE=on
ENV GOPRIVATE=gitlab.com


WORKDIR /opt/wallet-backend
ADD . .
RUN CGO_ENABLED=0 go build .

# Final stage
FROM alpine:3.7

# Allow delve to run on Alpine based containers.
RUN apk add --no-cache ca-certificates bash

WORKDIR /opt/wallet-backend

COPY --from=build-env /opt/wallet-backend/proxy-back ./

# rest api
EXPOSE 3000
# prometheus metrics
EXPOSE 20000

#CMD ./proxy-back -config config.yaml
