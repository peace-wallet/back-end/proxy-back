package rpc

import (
	"gitlab.com/jaxnet/jaxnetd/network/jaxrpc"
	"gitlab.com/peace-wallet/back-end/proxy-back/config"
)

func AcquireJAXClient(shardID uint32) (client *jaxrpc.Client, err error) {
	return newJAXClient(shardID)
}

func newJAXClient(shardID uint32) (*jaxrpc.Client, error) {
	shardConf, err := config.Config.Blockchain.JAX.Config(shardID)
	if err != nil {
		return nil, err
	}

	conf := &jaxrpc.ConnConfig{
		Hosts: []jaxrpc.Host{
			{
				Host: shardConf.Interface(),
				User: shardConf.User,
				Pass: shardConf.Pass,
			}},
		DisableTLS: true,
		Params:     config.Config.Blockchain.JAX.Name,
	}

	c, err := jaxrpc.New(conf)
	return c, err
}
