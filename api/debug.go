package api

import (
	"errors"
	"fmt"

	"github.com/valyala/fasthttp"
	"gitlab.com/peace-wallet/back-end/proxy-back/config"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
)

func emulateErrorCodeResponseIfNeeded(ctx *fasthttp.RequestCtx) (reportedError error) {
	if config.Config.Verbosity != "DEBUG" {
		return
	}

	debugArg := ctx.QueryArgs().Peek("debug")

	switch string(debugArg) {
	case fmt.Sprint(errCodeOrphanTransaction):
		reportedError = errors.New("can't publish TX into the network: (emulated)")
		ec.AssignErrorAndHttpCode(&reportedError, errCodeOrphanTransaction, fasthttp.StatusInternalServerError)
		return

	case fmt.Sprint(errCodeDuplicatedTransaction):
		reportedError = errors.New("can't publish TX into the network: (emulated)")
		ec.AssignErrorAndHttpCode(&reportedError, errCodeDuplicatedTransaction, fasthttp.StatusInternalServerError)
		return

	case fmt.Sprint(errCodeOrphanTransactionMemPool):
		reportedError = errors.New("can't publish TX into the network: (emulated)")
		ec.AssignErrorAndHttpCode(&reportedError, errCodeOrphanTransactionMemPool, fasthttp.StatusInternalServerError)
		return

	case fmt.Sprint(errCodeOrphanTransactionMemPool):
		reportedError = errors.New("can't publish TX into the network: (emulated)")
		ec.AssignErrorAndHttpCode(&reportedError, errCodeOrphanTransactionMemPool, fasthttp.StatusInternalServerError)
		return

	case fmt.Sprint(errNoFreeUTXOLeft):
		reportedError = errors.New("no free UTXOs are available: (emulated)")
		ec.AssignErrorAndHttpCode(&reportedError, errNoFreeUTXOLeft, fasthttp.StatusInternalServerError)
		return
	}

	return
}
