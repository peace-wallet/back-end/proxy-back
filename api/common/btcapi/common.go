package btcapi

import (
	"encoding/json"
	"fmt"
	http2 "net/http"

	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/httpx"
	"gitlab.com/peace-wallet/back-end/proxy-back/config"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
)

func GetOneItem(scope, urlPart string) (data map[string]interface{}, statusCode int, err error) {
	responseData, statusCode, err := getData(scope, urlPart)
	if err != nil {
		return
	}

	if statusCode == http2.StatusOK {
		item, ok := responseData["item"].(map[string]interface{})
		if !ok {
			err = ec.EmitError("(no item field) unexpected response returned from BTC API: %+v", &responseData)
			return
		}

		data = item
		return
	}

	return
}

func GetListOfItems(scope, urlPart string) (data []interface{}, statusCode int, err error) {
	responseData, statusCode, err := getData(scope, urlPart)
	if err != nil {
		return
	}

	if statusCode == http2.StatusOK {

		items, ok := responseData["items"].([]interface{})
		if !ok {
			err = ec.EmitError("(no items field) unexpected response returned from BTC API: %+v", &responseData)
			return
		}

		data = items
		return
	}

	return
}

func PostAndRetrieveOneItem(scope, urlPart string, data interface{}) (response map[string]interface{}, err error) {
	responseData, err := postData(scope, urlPart, data)
	if err != nil {
		return
	}

	item, ok := responseData["item"].(map[string]interface{})
	if !ok {
		err = ec.EmitError("(no item field) unexpected response returned from BTC API: %+v", &responseData)
		return
	}

	response = item
	return
}

func getData(scope, urlPart string) (data map[string]interface{}, statusCode int, err error) {
	var (
		url        string
		binaryData []byte
		ok         bool
	)
	url, err = buildURL(scope, urlPart)
	if err != nil {
		return
	}

	binaryData, statusCode, err = httpx.GetOrRetry(url, []httpx.Header{{Key: "X-Api-Key", Value: config.Config.Blockchain.BTC.APIKey}})
	if err != nil {
		return
	}

	switch statusCode {
	case http2.StatusOK:
		response := make(map[string]interface{})
		err = json.Unmarshal(binaryData, &response)
		if err != nil {
			err = ec.EmitError("can't unmarshal API response (response: %s)", binaryData)
			return
		}

		data, ok = response["data"].(map[string]interface{})
		if !ok {
			err = ec.EmitError("can't unmarshal API response (response: %s)", binaryData)
			return
		}

		return

	case http2.StatusNotFound:
		// Do not parse response.
		return

	default:
		err = fmt.Errorf("unexpected response from API: %s", string(binaryData))
		return
	}
}

func postData(scope, urlPart string, data interface{}) (response map[string]interface{}, err error) {
	url, err := buildURL(scope, urlPart)
	if err != nil {
		return
	}

	dataJSON, err := json.Marshal(data)
	if err != nil {
		err = ec.EmitError("can't marshall request: %+v", data)
		return
	}

	binaryData, err := httpx.DoPostRequest(url, dataJSON, []httpx.Header{{Key: "X-Api-Key", Value: config.Config.Blockchain.BTC.APIKey}})
	if err != nil {
		return
	}

	response = make(map[string]interface{})
	err = json.Unmarshal(binaryData, &response)
	if err != nil {
		err = ec.EmitError("can't unmarshal API response (response: %s)", binaryData)
		return
	}

	data, ok := response["data"].(map[string]interface{})
	if !ok {
		err = ec.EmitError("can't unmarshal API response (response: %s)", binaryData)
		return
	}

	response = data.(map[string]interface{})
	return
}

func buildURL(scope, urlPart string) (url string, err error) {
	network, err := getNetwork()
	if err != nil {
		return
	}

	url = "https://rest.cryptoapis.io/" + scope + "/bitcoin/" + network + "/" + urlPart
	return
}

func getNetwork() (network string, err error) {
	switch config.Config.Blockchain.BTC.Name {
	case "mainnet":
		network = "mainnet"

	case "testnet3", "testnet":
		network = "testnet"

	default:
		err = ec.EmitError("unsupported network specified: API only supports `mainnet` and `testnet`")
		return
	}

	return
}
