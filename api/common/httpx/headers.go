package httpx

type Header struct {
	Key   string
	Value string
}
