package httpx

import (
	"fmt"
	"math/rand"

	jsoniter "github.com/json-iterator/go"
	"github.com/valyala/fasthttp"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
	"gitlab.com/peace-wallet/back-end/proxy-back/logger"
)

func HandleErrors(internalErr, reportedErr *error, ctx *fasthttp.RequestCtx) {
	if *reportedErr != nil {
		discloseError(*reportedErr, ctx, fasthttp.StatusBadRequest)
		if *internalErr != nil {
			logger.Log.Err(*internalErr).Str(
				"paired-err", (*reportedErr).Error()).Str(
				"request", ctx.Request.String()).Msg(
				"Paired error to the reported error occurred")
		}

	} else {
		if *internalErr != nil {
			handleErrorAndHideDetails(*internalErr, ctx)
		}
	}
}

// discloseError is used for the cases when error can be shown to the caller.
func discloseError(err error, ctx *fasthttp.RequestCtx, status int) {

	ctx.Response.Reset()
	httpError, ok := err.(*ec.HttpErr)
	if ok {
		ctx.SetStatusCode(httpError.HttpStatusCode)

		data, err := jsoniter.Marshal(httpError)
		if err != nil {
			return
		}

		ctx.SetBody(data)
		ctx.SetContentType("application/json")
		return

	} else {
		errorID := rand.Int31()
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.SetBody([]byte(fmt.Sprintf(`{"code":-1,"message":"unexpected error, id=%d"}`, errorID)))
		ctx.SetContentType("application/json")
		return
	}
}

// handleErrorAndHideDetails is used in the cases when error must be logged but no details must be shared with user.
func handleErrorAndHideDetails(err error, ctx *fasthttp.RequestCtx) {
	errorID := rand.Int31()

	logger.Log.Err(err).Int32(
		"error-id", errorID).Str(
		"request", ctx.Request.String()).Msg(
		"error occurred during processing http request")

	ctx.Response.Reset()
	ctx.SetStatusCode(fasthttp.StatusInternalServerError)
	ctx.SetBody([]byte(fmt.Sprintf(`{"code":-1,"message":"unexpected error, id=%d"}`, errorID)))
	ctx.SetContentType("application/json")
	return
}
