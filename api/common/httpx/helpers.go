package httpx

import (
	"errors"
	"fmt"
	"time"

	"github.com/valyala/fasthttp"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
)

func DoGetRequest(url string) (data []byte, err error) {
	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req)
	req.SetRequestURI(url)

	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(resp)

	client := &fasthttp.Client{}
	err = client.Do(req, resp)
	if err != nil {
		return
	}

	if resp.StatusCode() != fasthttp.StatusOK {
		err = errors.New("bad gateway")
		return
	}

	data = resp.Body()[:]
	return
}

func DoPostRequest(url string, payload []byte, headers []Header) (data []byte, err error) {
	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req)
	req.SetRequestURI(url)
	req.SetBody(payload)
	req.Header.SetMethod("POST")
	for _, header := range headers {
		req.Header.Set(header.Key, header.Value)
	}
	req.Header.Set("Content-Type", "application/json")

	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(resp)

	client := &fasthttp.Client{}
	err = client.Do(req, resp)
	if err != nil {
		return
	}

	if resp.StatusCode() != fasthttp.StatusOK {
		err = fmt.Errorf("gateway returned unexpected error: %s (status code %d)", resp.Body(), resp.StatusCode())
		return
	}

	data = resp.Body()[:]
	return
}

func ProxyGetJSON(ctx *fasthttp.RequestCtx, url string) (err error) {
	data, err := DoGetRequest(url)
	if err != nil {
		return
	}

	ctx.SetBody(data)
	ctx.Response.Header.SetContentType("application/json")
	return
}

func ProxyGetJSONWithDataWrapper(ctx *fasthttp.RequestCtx, url string) (err error) {
	data, err := DoGetRequest(url)
	if err != nil {
		return
	}

	response := fmt.Sprint(`{"data":`, string(data), `}`)
	_, err = ctx.Write([]byte(response))
	if err != nil {
		return
	}

	ctx.Response.Header.SetContentType("application/json")
	return
}

// GetOrRetry makes a GET request to the specified URL and with specified headers.
// `headers` could be empty.
// In case of any error — the requests would be retried automatically for several times with longer timeout.
// This method unifies the source code entry point for working with fasthttp requests and responses,
// and allows to hide details about fasthttp's pool of requests and responses.
func GetOrRetry(url string, headers []Header) (data []byte, statusCode int, err error) {
	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req)
	req.SetRequestURI(url)

	for _, header := range headers {
		req.Header.Set(header.Key, header.Value)
	}

	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(resp)

	client := &fasthttp.Client{}
	err = client.Do(req, resp)
	if err != nil {
		for attempt := 1; attempt <= 3; attempt++ {
			time.Sleep(time.Second * time.Duration(attempt))
			err = client.Do(req, resp)
			if err == nil {
				break
			}
		}
	}
	if err != nil {
		err = ec.EmitError(
			"HTTP request to %s failed (request headers: %+v, response: %s, status code: %d)",
			url, headers, resp.String(), resp.StatusCode())

		return
	}

	data = resp.Body()[:]
	statusCode = resp.StatusCode()
	return
}
