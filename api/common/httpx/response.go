package httpx

import (
	jsoniter "github.com/json-iterator/go"
	"github.com/valyala/fasthttp"
)

type Response struct {
	Data interface{} `json:"data"`
}

func WriteJSONResponse(ctx *fasthttp.RequestCtx, response *Response) (err error) {
	return WriteJSONResponseAny(ctx, response)
}

func WriteJSONResponseAny(ctx *fasthttp.RequestCtx, response any) error {
	data, err := jsoniter.Marshal(response)
	if err != nil {
		return err
	}

	_, err = ctx.Write(data)
	if err != nil {
		return err
	}

	ctx.Response.Header.SetContentType("application/json")
	return err
}
