package httpx

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
	jaxutils "gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/peace-wallet/back-end/proxy-back/config"
)

func ParseURLParamToTime(ctx *fasthttp.RequestCtx, argName string) (t time.Time, err error) {
	return time.Parse(time.RFC3339, string(ctx.QueryArgs().Peek(argName)))
}

func ParseURLParamToUint64(ctx *fasthttp.RequestCtx, argName string) (i uint64, err error) {
	return strconv.ParseUint(string(ctx.QueryArgs().Peek(argName)), 10, 64)
}

func GetShardIDFromPath(ctx *fasthttp.RequestCtx) (shardID uint32, err error) {
	const argument = "shard-id"

	arg, err := getArgumentFromPath(argument, ctx)
	if err != nil {
		return
	}

	var tmpShardID int
	tmpShardID, err = strconv.Atoi(arg)
	if err != nil {
		err = fmt.Errorf("invalid argument `%s`", argument)
		return
	}

	shardID = uint32(tmpShardID)
	return
}

func GetHashFromPath(ctx *fasthttp.RequestCtx) (hash *chainhash.Hash, err error) {
	const argument = "hash"

	arg, err := getArgumentFromPath(argument, ctx)
	if err != nil {
		return
	}

	hash, err = chainhash.NewHashFromStr(arg)
	return
}

func GetHashesHexListFromParams(ctx *fasthttp.RequestCtx) (hashList []string, err error) {
	const argument = "hash"

	hashListB := ctx.QueryArgs().PeekMulti(argument)
	for _, hashB := range hashListB {
		hash, err := chainhash.NewHashFromStr(string(hashB))
		if err != nil {
			continue
		}
		hashList = append(hashList, hash.String())
	}

	if len(hashList) == 0 {
		err = errors.New("there are no hashes")
	}
	return
}

func GetJaxAddressFromPath(ctx *fasthttp.RequestCtx) (address jaxutils.Address, err error) {
	const argument = "address"

	arg, err := getArgumentFromPath(argument, ctx)
	if err != nil {
		return
	}

	networkCfg, err := config.Config.Blockchain.JAX.NetworkCfg()
	if err != nil {
		return
	}

	address, err = jaxutils.DecodeAddress(arg, networkCfg)
	if err != nil {
		err = fmt.Errorf("can't decode address: %w", err)
		return
	}

	return
}

func getArgumentFromPath(parameter string, ctx *fasthttp.RequestCtx) (data string, err error) {
	arg := ctx.UserValue(parameter)

	data, ok := arg.(string)
	if !ok {
		err = fmt.Errorf("parameter `%s` is required ", parameter)
		return
	}

	if data == "" {
		err = fmt.Errorf("parameter `%s` is required ", parameter)
		return
	}

	return
}
