package api

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/jaxnetd/network/jaxrpc"
	jaxwire "gitlab.com/jaxnet/jaxnetd/types/wire"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/httpx"
	"gitlab.com/peace-wallet/back-end/proxy-back/config"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
	"gitlab.com/peace-wallet/back-end/proxy-back/logger"
	"gitlab.com/peace-wallet/back-end/proxy-back/rpc"
)

type PublishingTxRequest struct {
	TxHex string `json:"transaction"`
}

type PublishingTxResponse struct {
	Hash string `json:"hash"`
}

type utxo struct {
	Hash   string `json:"hash"`
	Output uint32 `json:"output"`
}

// TransactionPublishing allows to publish TX into the network (JAX or BTC).
// For JAX-related TXs it also marks UTXOs used in transaction as used for 2 blocks (UTXOs orphans prevention).
//
// Deprecated
// in favour of v2 API that would not block UTXOs even for JAX network
// and would delegate this to the mempool sync instead of dealing with external indexes.
func TransactionPublishing(ctx *fasthttp.RequestCtx) {
	var (
		err           error
		reportedError error
		shardID       uint32
		hash          string
		txBinary      []byte
	)
	defer httpx.HandleErrors(&err, &reportedError, ctx)

	reportedError = emulateErrorCodeResponseIfNeeded(ctx)
	if reportedError != nil {
		return
	}

	shardID, reportedError = httpx.GetShardIDFromPath(ctx)
	if reportedError != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidShardID)
		return
	}

	if shardID == ec.BTCShardIndex {
		ec.AssignErrorCode(&reportedError, errCodeInvalidShardID)
		return
	}

	data := PublishingTxRequest{}
	err = json.Unmarshal(ctx.Request.Body(), &data)
	if err != nil {
		reportedError = errors.New("invalid request body")
		ec.AssignErrorCode(&reportedError, errCodeInvalidRequestBody)
		return
	}

	logger.Log.Debug().Str("tx-hex", data.TxHex).Uint32("shard-id", shardID).Msg("Tx Publishing")
	txBinary, err = hex.DecodeString(data.TxHex)
	if err != nil {
		reportedError = errors.New("invalid transaction hex representation")
		ec.AssignErrorCode(&reportedError, errCodeInvalidTxHex)
		return
	}

	var (
		tx jaxwire.MsgTx
	)

	err = tx.Deserialize(bytes.NewReader(txBinary))
	if err != nil {
		reportedError = errors.New("invalid transaction hex representation")
		ec.AssignErrorCode(&reportedError, errCodeInvalidTxHex)
		return
	}

	//usedUTXOs = make([]utxo, 0, len(tx.TxIn))
	//for _, in := range tx.TxIn {
	//	u := utxo{
	//		Hash:   in.PreviousOutPoint.Hash.String(),
	//		Output: in.PreviousOutPoint.Index,
	//	}
	//
	//	usedUTXOs = append(usedUTXOs, u)
	//}

	client, err := rpc.AcquireJAXClient(shardID)
	if err != nil {
		return
	}
	hash, err = publishJaxTx(client, shardID, &tx)

	if err != nil {
		logTxPubError(err, data.TxHex, shardID)

		if strings.Contains(err.Error(), "orphan transaction") {
			//markReportedOrphanUTXOsAsSuspicious(shardID, err, usedUTXOs)
			reportedError = errors.New("can't publish TX into the network: " + err.Error())
			ec.AssignErrorAndHttpCode(&reportedError, errCodeOrphanTransaction, fasthttp.StatusInternalServerError)

		} else if strings.Contains(err.Error(), "already have transaction") {
			reportedError = errors.New("can't publish TX into the network: " + err.Error())
			ec.AssignErrorAndHttpCode(&reportedError, errCodeDuplicatedTransaction, fasthttp.StatusInternalServerError)

		} else if strings.Contains(err.Error(), "already spent by transaction") {
			//markReportedOrphanUTXOsAsSuspicious(shardID, err, usedUTXOs)
			reportedError = errors.New("can't publish TX into the network: " + err.Error())
			ec.AssignErrorAndHttpCode(&reportedError, errCodeOrphanTransactionMemPool, fasthttp.StatusInternalServerError)

		} else if strings.Contains(err.Error(), "is invalid for stack size 0") {
			reportedError = errors.New("can't publish TX into the network: " + err.Error())
			ec.AssignErrorAndHttpCode(&reportedError, errRefundLockDoesNotElapsed, fasthttp.StatusInternalServerError)

		} else {
			reportedError = errors.New(
				"can't publish TX into the network (invalid/corrupted transaction or network communication issues)")
			ec.AssignErrorAndHttpCode(&reportedError, errCodeCantPublishTX, fasthttp.StatusInternalServerError)
		}

		return
	}

	// TX published.
	// Now used UTXOs must be marked on index's side to prevent their occurrence in consequent UTXOs queries.
	//err = lockUTXOs(shardID, usedUTXOs)
	//if err != nil {
	//	return
	//}

	response := &httpx.Response{
		Data: &PublishingTxResponse{Hash: hash},
	}

	err = httpx.WriteJSONResponse(ctx, response)
}

func publishJaxTx(client *jaxrpc.Client, shardID uint32, tx *jaxwire.MsgTx) (hash string, err error) {
	h, err := client.SendRawTransaction(shardID, tx)
	if err != nil {
		// One more attempt
		time.Sleep(time.Second)

		h, err = client.SendRawTransaction(shardID, tx)
		if err != nil {
			return
		}
	}

	hash = h.String()
	return
}

// markReportedOrphanUTXOsAsSuspicious marks each UTXO for `utxos` as invalid
// and prevents it from being returned by the index once more.
func markReportedOrphanUTXOsAsSuspicious(shardID uint32, reportedErr error, utxos []utxo) {
	// Example of error:
	// error="-25: TX rejected: orphan transaction fd3a3a42f60f328372aa29f940e6bc479126d400965bcaca72a30a571f8b0dd9
	// references outputs of unknown or fully-spent transaction
	// 46e754d79f5d7aee71c17fac06284371de189aeabcefab8f0f92ab2367efb19d
	//
	// So, the error reports only one Hash of the orphan UTXO and one Hash of the TX itself.
	const expectedHashesCount = 2

	re := regexp.MustCompile("[a-f0-9]{64}")
	hashesFound := re.FindAllString(reportedErr.Error(), expectedHashesCount)

	for _, hash := range hashesFound {
		for _, u := range utxos {
			if u.Hash == hash {
				err := disableUTXOs(shardID, utxos)
				if err != nil {
					log.Err(err).Msg("Can't mark orphan UTXOs in index")
				} else {
					log.Warn().Str(
						"context", "transaction-publishing").Str(
						"reason", reportedErr.Error()).Str(
						"hash", hash).Msg(
						"Suspicious UTXO disabled from being accessed (on index's side)")
				}
			}
		}
	}
}

func lockUTXOs(shardID uint32, utxos []utxo) (err error) {
	payload, err := json.Marshal(utxos)
	if err != nil {
		return
	}

	url := fmt.Sprintf("%s/api/v1/shards/%d/indexes/utxos/deactivate/", config.Config.Explorer.HostURL(), shardID)
	_, err = httpx.DoPostRequest(url, payload, nil)
	if err != nil {
		return
	}

	log.Trace().Str("request-payload", string(payload)).Msg("UTXOs locked on index's side")
	return
}

func disableUTXOs(shardID uint32, utxos []utxo) (err error) {
	return nil

	//payload, err := json.Marshal(utxos)
	//if err != nil {
	//	return
	//}
	//
	//url := fmt.Sprintf("%s/api/v1/shards/%d/indexes/utxos/deactivate/", config.Config.Explorer.HostURL(), shardID)
	//_, err = httpx.DoPostRequest(url, payload, nil)
	//if err != nil {
	//	return
	//}
	//
	//log.Trace().Str("request-payload", string(payload)).Msg("UTXOs disabled on index's side")
	//return
}

func logTxPubError(err error, txHex string, shardID uint32) {
	logger.Log.Err(err).Str(
		"ctx", "tx-publishing").Str(
		"tx-hex", txHex).Uint32(
		"shard-id", shardID).Msg(
		"TX publishing failed")
}
