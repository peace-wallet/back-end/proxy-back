package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/btcapi"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/httpx"
	"gitlab.com/peace-wallet/back-end/proxy-back/config"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
	"gitlab.com/peace-wallet/back-end/proxy-back/rpc"
	"gitlab.com/peace-wallet/back-end/proxy-back/utils"
	"gitlab.com/peace-wallet/back-end/proxy-back/utils/fees"
)

const (
	// Right not JAX it is not safe to create TXs with more than 256 UTXOs in it.
	// The TX could be rejected by the node.
	maxUTXOsCount = 256
)

type UTXO struct {
	TxHash   string `json:"hash"`
	TxOutput uint16 `json:"output"`
	Amount   int64  `json:"amount"`
	PKScript string `json:"pkScript,omitempty"`
}

type UTXOsIndexerResponse struct {
	Data []UTXO `json:"data"`
}

type Fee struct {
	Fee       int64 `json:"fee"`
	MaxAmount int64 `json:"maxAmount"`
}

type FeeResponse struct {
	Slow     Fee `json:"slow"`
	Moderate Fee `json:"moderate"`
	Fast     Fee `json:"fast"`
}

type UTXOsResponse struct {
	Code  int         `json:"code"`
	UTXOS []UTXO      `json:"utxos"`
	Fees  FeeResponse `json:"fees"`
}

func V2AddressUTXOs(ctx *fasthttp.RequestCtx) {
	var (
		err, reportedError error
		address            jaxutil.Address
		shardID            uint32
		strategy           string
		amount             int64
	)

	defer httpx.HandleErrors(&err, &reportedError, ctx)

	reportedError = emulateErrorCodeResponseIfNeeded(ctx)
	if reportedError != nil {
		return
	}

	address, reportedError = httpx.GetJaxAddressFromPath(ctx)
	if reportedError != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidAddress)
		return
	}

	shardID, reportedError = httpx.GetShardIDFromPath(ctx)
	if reportedError != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidShardID)
		return
	}

	// Parameters "amount", "pubKeyHex", "sigHex" would be validated on the index's side.
	// Corresponding JSON error would be returned in case of error.
	amountStr := string(ctx.QueryArgs().Peek("amount"))
	if amountStr == "" {
		ec.AssignErrorCode(&reportedError, errCodeInvalidAmount)
		return
	}

	amount, reportedError = strconv.ParseInt(amountStr, 10, 64)
	if err != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidAmount)
		return
	}

	strategy = string(ctx.QueryArgs().Peek("strategy"))
	switch strategy {
	case "":
		// By default (if parameter is omitted).
		strategy = "biggest-first"

	case "smallest-first", "biggest-first":
		// OK, passed one of expected values.
		break

	default:
		ec.AssignErrorCode(&reportedError, errCodeInvalidUTXOFetchingStrategy)
		return
	}

	getUTXOSForAmount := func(requiredAmount int64) (UTXOs []UTXO, err error) {
		if shardID == ec.BTCShardIndex {
			return queryBTCApiForUTXOs(address.EncodeAddress(), requiredAmount)
		} else {
			return queryIndexForUTXOs(address.EncodeAddress(), requiredAmount, shardID, strategy)
		}
	}

	writeResponse := func(utxosList []UTXO, totalUTXOsAmountAggregated, feeSlow, feeModerate, feeFast int64) {
		slowFee := utils.EstimateFee(len(utxosList), 1, feeSlow, true, shardID)
		moderateFee := utils.EstimateFee(len(utxosList), 1, feeModerate, true, shardID)
		fastFee := utils.EstimateFee(len(utxosList), 1, feeFast, true, shardID)
		responseData := &UTXOsResponse{
			Code:  0,
			UTXOS: utxosList,
			Fees: FeeResponse{
				Slow: Fee{
					Fee:       slowFee,
					MaxAmount: maxAmountFunc(amount, slowFee, totalUTXOsAmountAggregated),
				},
				Moderate: Fee{
					Fee:       moderateFee,
					MaxAmount: maxAmountFunc(amount, moderateFee, totalUTXOsAmountAggregated),
				},
				Fast: Fee{
					Fee:       fastFee,
					MaxAmount: maxAmountFunc(amount, fastFee, totalUTXOsAmountAggregated),
				},
			},
		}
		err = httpx.WriteJSONResponse(ctx, &httpx.Response{Data: responseData})
	}

	nodeFeeFast, nodeFeeModerate, nodeFeeSlow, err := fetchFeesConfiguration(shardID)
	if err != nil {
		return
	}

	var (
		// By default, it is set to 0.
		// In 95% of cases, aggregated amount of UTXOs fetched,
		// would be greater than required amount, and fee (even fast) could be covered.
		// In some rare cases, one (or in very rare cases, few) more UTXOs fetching round(s) would be required.
		fastFee int64

		previousFetchingRoundUTXOsCount int
	)
	for {
		var (
			UTXOs              []UTXO
			UTXOsAggregatedSum int64
		)

		UTXOs, err = getUTXOSForAmount(amount + fastFee)
		if len(UTXOs) == 0 || err != nil {
			reportedError = errors.New("no free UTXOs are available")
			ec.AssignErrorAndHttpCode(&reportedError, errNoFreeUTXOLeft, fasthttp.StatusInternalServerError)
			return
		}

		// Checking if fetched UTXOs are enough to cover most expensive fee.
		// If not — the new set of UTXOs need to be fetched for increased amount.
		// The complexity here is that fee is calculate per UTXOs count in TX,
		// so it is required to fetch the UTXOs list and only then it is possible to calculate the fees.
		// In some cases, fee does not fit into UTXOs fetched, so the new set must be fetched.
		for _, record := range UTXOs {
			UTXOsAggregatedSum += record.Amount
		}

		fastFee = utils.EstimateFee(len(UTXOs), 1, nodeFeeFast, true, shardID)
		if UTXOsAggregatedSum >= amount+fastFee || previousFetchingRoundUTXOsCount == len(UTXOs) || len(UTXOs) == maxUTXOsCount {
			writeResponse(UTXOs, UTXOsAggregatedSum, nodeFeeSlow, nodeFeeModerate, nodeFeeFast)
			return

		} else {
			previousFetchingRoundUTXOsCount = len(UTXOs)
		}
	}
}

func queryBTCApiForUTXOs(address string, requiredAmount int64) (UTXOs []UTXO, err error) {
	const (
		expectedUTXOsPerRequest = 20
	)

	var (
		offset               = 0
		limit                = expectedUTXOsPerRequest
		totalAmountCollected int64
		records              []interface{}
		amountFloat          float64
	)

	// Set UTXOs slice cap to the expected records per request.
	// In 95% total amount of UTXOs fetched would not exceed this room.
	//
	// Setting the cap initially allows to prevent memory fragmentation,
	// because on each request this slice would be initialized with the same size memory chunk,
	// which could be easily handled and reused by the GC.
	UTXOs = make([]UTXO, 0, expectedUTXOsPerRequest)

	for offset = 0; true; offset += limit {
		urlPart := fmt.Sprintf("addresses/%s/unspent-outputs?offset=%d&limit=%d", address, offset, limit)
		records, _, err = btcapi.GetListOfItems("blockchain-data", urlPart)
		if err != nil {
			err = fmt.Errorf("BTC API call for UTXOs failed: %w (address — %s, amount — %d)", err, address, requiredAmount)
			return
		}

		if len(records) == 0 {
			// No more UTXOs left for this address.
			return

		} else {
			for _, record := range records {
				utxo, ok := record.(map[string]interface{})
				if !ok {
					err = fmt.Errorf("invalid BTC API response: can't convert interface to map")
					return
				}

				isAvailableRaw, availableIsPresent := utxo["isAvailable"]
				amountRaw, amountIsPresent := utxo["amount"]
				indexRaw, indexIsPresent := utxo["index"]
				hashRaw, hashIsPresent := utxo["transactionId"]

				if !availableIsPresent || !amountIsPresent || !indexIsPresent || !hashIsPresent {
					err = fmt.Errorf("invalid BTC API response: one of required field is missed")
					return
				}

				isAvailable, isAvailableIsConverted := isAvailableRaw.(bool)
				amountString, amountIsConverted := amountRaw.(string)
				index, indexIsConverted := indexRaw.(float64)
				hash, hashIsConverted := hashRaw.(string)

				if !isAvailableIsConverted || !amountIsConverted || !indexIsConverted || !hashIsConverted {
					err = fmt.Errorf("invalid BTC API response: one of required field could not be converted to the expected type")
					return
				}

				if !isAvailable {
					// UTXO could not be used in TX.
					// Potentially used in another non-confirmed TX.
					continue
				}

				amountFloat, err = strconv.ParseFloat(amountString, 64)
				if err != nil {
					err = fmt.Errorf("invalid BTC API response: can't parse UTXO amount (conversion to float failed)")
					return
				}

				amount := int64(amountFloat * 100000000)
				UTXOs = append(UTXOs, UTXO{
					TxHash:   hash,
					TxOutput: uint16(index),
					Amount:   amount,
					PKScript: "", // BTC API does not provide PK script info.
				})

				totalAmountCollected += amount
				if totalAmountCollected >= requiredAmount {
					return
				}
			}

			if len(records) < expectedUTXOsPerRequest {
				// Current set of UTXOs returned contains fewer items than was requested, by the limit parameter,
				// which means the is not free UTXOs are left fo this address and there is no need to do next request.
				return
			}
		}
	}

	return
}

func queryIndexForUTXOs(address string, requiredAmount int64, shardID uint32, strategy string) ([]UTXO, error) {
	const expectedUTXOsPerRequest = 100

	var (
		offset               = 0
		limit                = expectedUTXOsPerRequest
		totalAmountCollected int64
	)

	var result = make([]UTXO, 0, expectedUTXOsPerRequest)

	for offset = 0; true; offset += limit {
		// It is safe to call HTTP here:
		// this code is intended to be called in closed contour only.
		url := fmt.Sprintf("%s/api/v1/shards/%d/indexes/utxos/non-spent/?strategy=%s&address=%s&amount=%d&offset=%d&limit=%d",
			config.Config.Explorer.HostURL(), shardID, strategy, address, requiredAmount, offset, limit)

		data, err := httpx.DoGetRequest(url)
		if err != nil {
			for attempt := 1; attempt <= 3; attempt++ {
				time.Sleep(time.Millisecond * 200 * time.Duration(attempt))
				data, err = httpx.DoGetRequest(url)
				if err == nil {
					break
				}
			}
		}
		if err != nil {
			err = fmt.Errorf("can't fetch UTXOs from index: %w", err)
			return nil, err
		}

		var indexerResponse []UTXO
		err = json.Unmarshal(data, &indexerResponse)
		if err != nil {
			err = fmt.Errorf("invalid response from UTXOs index: %w", err)
			return nil, err
		}

		if len(indexerResponse) == 0 {
			// No more UTXOs left for this address.
			return nil, nil

		}

		for _, UTXO := range indexerResponse {
			result = append(result, UTXO)

			totalAmountCollected += UTXO.Amount
			if totalAmountCollected >= requiredAmount {
				return nil, err

			}

			if len(result) >= maxUTXOsCount {
				return result, nil
			}
		}

		if len(indexerResponse) < expectedUTXOsPerRequest {
			// Current set of UTXOs returned contains fewer items than was requested, by the limit parameter,
			// which means the is not free UTXOs are left fo this address and there is no need to do next request.
			return result, nil

		}
	}

	return result, nil
}

func fetchFeesConfiguration(shardID uint32) (fast, moderate, slow int64, err error) {
	var (
		jaxFees *jaxjson.ExtendedFeeFeeResult
	)

	if shardID != ec.BTCShardIndex {
		jaxClient, err := rpc.AcquireJAXClient(shardID)
		if err != nil {
			return fast, moderate, slow, err
		}

		jaxFees, err = jaxClient.GetExtendedFee(shardID)
		if err != nil {
			return fast, moderate, slow, err
		}

		fast = int64(jaxFees.Fast.SatoshiPerB)
		moderate = int64(jaxFees.Moderate.SatoshiPerB)
		slow = int64(jaxFees.Slow.SatoshiPerB)

	} else {
		btcFees := fees.GetActualFees()

		fast = int64(btcFees.Fast)
		moderate = int64(btcFees.Moderate)
		slow = int64(btcFees.Slow)
	}

	return
}

func maxAmountFunc(amount, fee, utxosSum int64) int64 {
	if utxosSum >= amount+fee {
		return amount
	} else {
		maxAmount := utxosSum - fee
		if maxAmount < 0 {
			maxAmount = 0
		}
		return maxAmount
	}
}
