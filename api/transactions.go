package api

import (
	"errors"
	"strings"

	"github.com/valyala/fasthttp"
	jaxhash "gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/httpx"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
	"gitlab.com/peace-wallet/back-end/proxy-back/rpc"
)

func Transactions(ctx *fasthttp.RequestCtx) {
	var (
		err, reportedError error
		shardID            uint32
	)
	defer httpx.HandleErrors(&err, &reportedError, ctx)

	shardID, reportedError = httpx.GetShardIDFromPath(ctx)
	if reportedError != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidShardID)
		return
	}

	if shardID == ec.BTCShardIndex {
		ec.AssignErrorCode(&reportedError, errCodeInvalidShardID)
		return
	}

	hashes, reportedError := httpx.GetHashesHexListFromParams(ctx)
	if reportedError != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidHash)
		return
	}

	reportedError, err = writeJaxResponse(ctx, shardID, hashes)
}

func writeJaxResponse(ctx *fasthttp.RequestCtx, shardID uint32, hashes []string) (reportedError, err error) {
	client, err := rpc.AcquireJAXClient(shardID)
	if err != nil {
		return nil, err
	}

	hashMap := make(map[string]string)
	for _, hash := range hashes {
		h, err := jaxhash.NewHashFromStr(hash)
		if err != nil {
			return nil, err
		}

		tx, err := client.GetTx(shardID, h, true)
		if err != nil {
			if strings.Contains(err.Error(), " No information available about transaction") {
				reportedError = errors.New("no TX found")
				ec.AssignErrorAndHttpCode(&reportedError, errCodeCantFoundTX, fasthttp.StatusNotFound)
				return reportedError, err
			}

			return nil, err
		}

		hashMap[h.String()] = tx.RawTx
	}

	response := &httpx.Response{Data: hashMap}
	err = httpx.WriteJSONResponse(ctx, response)
	return nil, err
}
