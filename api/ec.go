package api

const (
	errCodeInvalidShardID              = 1
	errCodeInvalidHash                 = 2
	errCodeInvalidAddress              = 3
	errCodeInvalidAmount               = 4
	errCodeInvalidFrom                 = 5
	errCodeInvalidTo                   = 6
	errCodeInvalidTxUUID               = 7
	errCodeInvalidEAD                  = 8
	errCodeInvalidToken                = 9
	errCodeInvalidTxInputIdx           = 10
	errCodeInvalidUTXOFetchingStrategy = 11
	errCodeInvalidLimit                = 12
	errCodeInvalidOffset               = 13
	errCodeInvalidState                = 14

	errCodeInvalidRequestArguments  = 99
	errCodeInvalidRequestBody       = 100
	errCodeInvalidTxHex             = 101
	errCodeCantPublishTX            = 102
	errCodeCantFoundTX              = 103
	errCodeOrphanTransaction        = 104
	errCodeDuplicatedTransaction    = 105
	errCodeOrphanTransactionMemPool = 106
	errNoFreeUTXOLeft               = 107
	errRefundLockDoesNotElapsed     = 108

	errCodeServerError = 1024

	BeaconBlockMiningTimeInSec = 600
	ShardBlockMiningTimeInSec  = 37
)
