package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"

	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/httpx"
	"gitlab.com/peace-wallet/back-end/proxy-back/config"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
	"gitlab.com/peace-wallet/back-end/proxy-back/rpc"
	"gitlab.com/peace-wallet/back-end/proxy-back/utils"
	"gitlab.com/peace-wallet/back-end/proxy-back/utils/fees"
)

func getUTXOSForAmount(shardID uint32, address string, amount int64, offset, limit int, strategy string) ([]UTXO, error) {
	url := fmt.Sprintf("%s/api/v1/shards/%d/indexes/utxos/non-spent/?strategy=%s&address=%s&amount=%d&offset=%d&limit=%d",
		config.Config.Explorer.HostURL(), shardID, strategy, address, amount, offset, limit)

	data, err := httpx.DoGetRequest(url)
	if err != nil {
		return nil, err
	}

	var result = struct {
		Data []UTXO `json:"data"`
	}{}

	err = json.Unmarshal(data, &result)
	if err != nil {
		return nil, err
	}

	return result.Data, nil
}
func AddressUTXOs(ctx *fasthttp.RequestCtx) {
	const maxUtxosCnt = 256
	var (
		err, reportedError error
		address            jaxutil.Address
		shardID            uint32
	)

	defer httpx.HandleErrors(&err, &reportedError, ctx)

	reportedError = emulateErrorCodeResponseIfNeeded(ctx)
	if reportedError != nil {
		return
	}

	address, reportedError = httpx.GetJaxAddressFromPath(ctx)
	if reportedError != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidAddress)
		return
	}

	shardID, reportedError = httpx.GetShardIDFromPath(ctx)
	if reportedError != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidShardID)
		return
	}

	// Parameters "amount", "pubKeyHex", "sigHex" would be validated on the index's side.
	// Corresponding JSON error would be returned in case of error.
	amountStr := string(ctx.QueryArgs().Peek("amount"))
	if amountStr == "" {
		amountStr = "0"
	}

	amount, reportedError := strconv.ParseInt(amountStr, 10, 64)
	if err != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidAmount)
		return
	}

	strategy := string(ctx.QueryArgs().Peek("strategy"))
	switch strategy {
	case "":
		// By default (if parameter is omitted).
		strategy = "biggest-first"

	case "smallest-first", "biggest-first":
		// OK, passed one of expected values.
		break

	default:
		ec.AssignErrorCode(&reportedError, errCodeInvalidUTXOFetchingStrategy)
		return
	}

	nodeFeeFast, nodeFeeModerate, nodeFeeSlow, err := v1FetchFeesConfiguration(shardID)
	if err != nil {
		return
	}

	var fastFee int64
	var prevUtxosCnt int

	for {

		var offset = 0
		var limit = 100
		var utxosSum int64 = 0
		var utxos []UTXO
		var indexerResponse []UTXO

		for ok := true; ok; ok = utxosSum < amount && len(indexerResponse) == limit {
			if len(utxos)+limit > maxUtxosCnt {
				limit = maxUtxosCnt - len(utxos)
			}

			indexerResponse, err = getUTXOSForAmount(shardID, address.String(),
				amount+fastFee-utxosSum, offset, limit, strategy)
			if err != nil {
				return
			}

			if len(indexerResponse) == 0 {
				break
			}

			utxos = append(utxos, indexerResponse...)

			for _, el := range indexerResponse {
				utxosSum += el.Amount
			}
			if len(utxos) >= maxUtxosCnt {
				break
			}
			offset += limit
		}

		if len(utxos) == 0 {
			reportedError = errors.New("no free UTXOs are available")
			ec.AssignErrorAndHttpCode(&reportedError, errNoFreeUTXOLeft, fasthttp.StatusInternalServerError)
			return
		}

		fastFee = utils.EstimateFee(len(utxos), 1, nodeFeeFast, true, shardID)
		if utxosSum >= amount+fastFee || prevUtxosCnt == len(utxos) || len(utxos) == maxUtxosCnt {
			responseData := &UTXOsResponse{
				Code:  0,
				UTXOS: utxos,
				Fees:  aggregateFee(shardID, len(utxos), amount, utxosSum, nodeFeeSlow, nodeFeeModerate, nodeFeeFast),
			}
			if len(utxos) >= maxUtxosCnt {
				responseData.Code = 1
			}
			err = httpx.WriteJSONResponse(ctx, &httpx.Response{Data: responseData})

			return

		}

		prevUtxosCnt = len(utxos)
	}
}

func aggregateFee(shardID uint32, utxoCount int, amount, utxosSum, slow, moderate, fast int64) FeeResponse {
	slowFee := utils.EstimateFee(utxoCount, 1, slow, true, shardID)
	moderateFee := utils.EstimateFee(utxoCount, 1, moderate, true, shardID)
	fastFee := utils.EstimateFee(utxoCount, 1, fast, true, shardID)
	return FeeResponse{
		Slow: Fee{
			Fee:       slowFee,
			MaxAmount: v1MaxAmountFunc(amount, slowFee, utxosSum),
		},
		Moderate: Fee{
			Fee:       moderateFee,
			MaxAmount: v1MaxAmountFunc(amount, moderateFee, utxosSum),
		},
		Fast: Fee{
			Fee:       fastFee,
			MaxAmount: v1MaxAmountFunc(amount, fastFee, utxosSum),
		},
	}
}

func v1FetchFeesConfiguration(shardID uint32) (fast, moderate, slow int64, err error) {
	var jaxFees *jaxjson.ExtendedFeeFeeResult

	if shardID != ec.BTCShardIndex {
		jaxClient, err := rpc.AcquireJAXClient(shardID)
		if err != nil {
			return fast, moderate, slow, err
		}

		jaxFees, err = jaxClient.GetExtendedFee(shardID)
		if err != nil {
			return fast, moderate, slow, err
		}

		fast = int64(jaxFees.Fast.SatoshiPerB)
		moderate = int64(jaxFees.Moderate.SatoshiPerB)
		slow = int64(jaxFees.Slow.SatoshiPerB)

	} else {
		btcFees := fees.GetActualFees()

		fast = int64(btcFees.Fast)
		moderate = int64(btcFees.Moderate)
		slow = int64(btcFees.Slow)
	}

	return
}

func v1MaxAmountFunc(amount, fee, utxosSum int64) int64 {
	if utxosSum >= amount+fee {
		return amount
	} else {
		maxAmount := utxosSum - fee
		if maxAmount < 0 {
			maxAmount = 0
		}
		return maxAmount
	}
}
