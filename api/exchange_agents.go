package api

import (
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/httpx"
	"gitlab.com/peace-wallet/back-end/proxy-back/rpc"
)

type EADAddress struct {
	URL       string    `json:"url"`
	IP        string    `json:"ip"`
	Port      uint16    `json:"port"`
	Expires   time.Time `json:"expires"`
	ShardsCnt uint8     `json:"-"`
}

type ExchangeAgent struct {
	ID        string       `json:"id"`
	PublicKey string       `json:"publicKey"`
	Addresses []EADAddress `json:"addresses"`
}

func ExchangeAgents(ctx *fasthttp.RequestCtx) {
	var (
		err, reportedError error
		parsedShard        uint64
		shards             []uint32
		response           = &httpx.Response{}
	)
	responseAgents := make([]ExchangeAgent, 0)
	defer httpx.HandleErrors(&err, &reportedError, ctx)

	parsedShard, reportedError = strconv.ParseUint(string(ctx.QueryArgs().Peek("sourceShardId")), 10, 32)
	if err != nil {
		return
	}
	shards = append(shards, uint32(parsedShard))

	parsedShard, reportedError = strconv.ParseUint(string(ctx.QueryArgs().Peek("destinationShardId")), 10, 32)
	if err != nil {
		return
	}
	shards = append(shards, uint32(parsedShard))

	client, err := rpc.AcquireJAXClient(0)
	if err != nil {
		return
	}

	agents, err := client.ListEADAddresses(shards, nil)
	if err != nil {
		return
	}

	for _, agent := range agents.Agents {
		var addresses []EADAddress
		for _, address := range agent.IPs {
			if address.Shard != shards[0] && address.Shard != shards[1] {
				continue
			}
			addresses = appendAddressIfNotExists(address, addresses)
		}
		for idx := 0; idx < len(addresses); {
			address := addresses[idx]
			if address.ShardsCnt != 2 {
				addresses = append(addresses[:idx], addresses[idx+1:]...)
			} else {
				idx++
			}
		}

		if len(addresses) > 0 {
			responseAgents = append(responseAgents, ExchangeAgent{
				ID:        strconv.FormatUint(agent.ID, 10),
				PublicKey: agent.PublicKey,
				Addresses: addresses,
			})
		}
	}

	response.Data = responseAgents
	err = httpx.WriteJSONResponse(ctx, response)
}

func appendAddressIfNotExists(addressForAppending jaxjson.EADAddress, addresses []EADAddress) (addressesNew []EADAddress) {
	for idx, address := range addresses {
		if address.IP == addressForAppending.IP && address.Port == addressForAppending.Port &&
			address.URL == addressForAppending.URL {
			addresses[idx].ShardsCnt++
			addressesNew = addresses
			return
		}
	}
	addresses = append(addresses, EADAddress{
		URL:       addressForAppending.URL,
		IP:        addressForAppending.IP,
		Port:      addressForAppending.Port,
		Expires:   addressForAppending.ExpiresAt,
		ShardsCnt: 1,
	})
	addressesNew = addresses
	return
}
