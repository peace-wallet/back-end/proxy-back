package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/valyala/fasthttp"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/btcapi"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/httpx"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
	"gitlab.com/peace-wallet/back-end/proxy-back/logger"
)

type V2PublishingTxResponse struct {
	ID string `json:"txID"`
}

type btcBroadcastRequestItem struct {
	SignedTransactionHex string `json:"signedTransactionHex"`
}

type btcBroadcastRequestData struct {
	Item btcBroadcastRequestItem `json:"item"`
}

type btcBroadcastRequest struct {
	Data btcBroadcastRequestData `json:"data"`
}

func V2TransactionPublish(ctx *fasthttp.RequestCtx) {
	var (
		err           error
		reportedError error
		shardID       uint32
		data          = PublishingTxRequest{}
		response      = &httpx.Response{
			Data: &V2PublishingTxResponse{}}
	)
	defer httpx.HandleErrors(&err, &reportedError, ctx)

	reportedError = emulateErrorCodeResponseIfNeeded(ctx)
	if reportedError != nil {
		return
	}

	shardID, reportedError = httpx.GetShardIDFromPath(ctx)
	if reportedError != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidShardID)
		return
	}

	// todo: add support of JAX/JXN txs publishing when mempool sync would be ready.
	if shardID != ec.BTCShardIndex {
		reportedError = fmt.Errorf("only BTC is supported by this method for now")
		ec.AssignErrorCode(&reportedError, errCodeInvalidShardID)
		return
	}

	err = json.Unmarshal(ctx.Request.Body(), &data)
	if err != nil {
		reportedError = errors.New("invalid request body")
		ec.AssignErrorCode(&reportedError, errCodeInvalidRequestBody)
		return
	}

	if shardID == ec.BTCShardIndex {
		txID, err := v2publishBtcTx(data.TxHex)
		if err != nil {
			if strings.Contains(err.Error(), "exists") {
				err = nil
				ctx.SetBody([]byte(`{"error": "tx is already published"}`))
				return
			}

			reportedError = errors.New("can't publish TX")
			return
		}

		response.Data = &V2PublishingTxResponse{ID: txID}
	} else {
		// txBinary, err = hex.DecodeString(data.TxHex)
		// if err != nil {
		// 	reportedError = errors.New("invalid transaction hex representation")
		// 	ec.AssignErrorCode(&reportedError, errCodeInvalidTxHex)
		// 	return
		// }

		// todo: continue with JAX TX
		// ...

		// if err != nil {
		// 	logTxPubError(err, data.TxHex, shardID)
		//
		// 	if strings.Contains(err.Error(), "orphan transaction") {
		// 		markReportedOrphanUTXOsAsSuspicious(shardID, err, usedUTXOs)
		// 		reportedError = errors.New("can't publish TX into the network: " + err.Error())
		// 		ec.AssignErrorAndHttpCode(&reportedError, errCodeOrphanTransaction, fasthttp.StatusInternalServerError)
		//
		// 	} else if strings.Contains(err.Error(), "already have transaction") {
		// 		reportedError = errors.New("can't publish TX into the network: " + err.Error())
		// 		ec.AssignErrorAndHttpCode(&reportedError, errCodeDuplicatedTransaction, fasthttp.StatusInternalServerError)
		//
		// 	} else if strings.Contains(err.Error(), "already spent by transaction") {
		// 		markReportedOrphanUTXOsAsSuspicious(shardID, err, usedUTXOs)
		// 		reportedError = errors.New("can't publish TX into the network: " + err.Error())
		// 		ec.AssignErrorAndHttpCode(&reportedError, errCodeOrphanTransactionMemPool, fasthttp.StatusInternalServerError)
		//
		// 	} else if strings.Contains(err.Error(), "is invalid for stack size 0") {
		// 		reportedError = errors.New("can't publish TX into the network: " + err.Error())
		// 		ec.AssignErrorAndHttpCode(&reportedError, errRefundLockDoesNotElapsed, fasthttp.StatusInternalServerError)
		//
		// 	} else {
		// 		reportedError = errors.New(
		// 			"can't publish TX into the network (invalid/corrupted transaction or network communication issues)")
		// 		ec.AssignErrorAndHttpCode(&reportedError, errCodeCantPublishTX, fasthttp.StatusInternalServerError)
		// 	}
		//
		// 	return
		// }

		// TX published.
		// Now used UTXOs must be marked on index's side to prevent their occurrence in consequent UTXOs queries.
		// err = lockUTXOs(shardID, usedUTXOs)
		// if err != nil {
		// 	return
		// }
	}

	err = httpx.WriteJSONResponse(ctx, response)
}

func v2publishBtcTx(hex string) (id string, err error) {
	logger.Log.Debug().Str("hex", hex).Msg("BTC TX publishing attempt")

	request := btcBroadcastRequest{
		Data: btcBroadcastRequestData{
			Item: btcBroadcastRequestItem{
				SignedTransactionHex: hex}}}

	item, err := btcapi.PostAndRetrieveOneItem("blockchain-tools", "transactions/broadcast", request)
	if err != nil {
		return
	}

	txID, isPresent := item["transactionId"]
	if !isPresent {
		err = ec.EmitError("unexpected response from BTC API: %+v", item)
		return
	}

	txIDConverted, ok := txID.(string)
	if !ok {
		err = ec.EmitError("unexpected response from BTC API: %+v", item)
		return
	}

	id = txIDConverted
	return
}
