package api

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/httpx"
	"gitlab.com/peace-wallet/back-end/proxy-back/config"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
)

type InputTxUsingStatus struct {
	IsUsed bool `json:"isUsed"`
}

func CheckInputTx(ctx *fasthttp.RequestCtx) {
	var (
		err, reportedError error
		txInIdx            int64
	)
	defer httpx.HandleErrors(&err, &reportedError, ctx)

	//address, reportedError = http.GetJaxAddressFromPath(ctx)
	//if reportedError != nil {
	//	ec.AssignErrorCode(&reportedError, errCodeInvalidAddress)
	//	return
	//}

	shardID, reportedError := httpx.GetShardIDFromPath(ctx)
	if reportedError != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidShardID)
		return
	}

	// Parameters "txHash", "txInIdx" would be validated on the index's side.
	// Corresponding JSON error would be returned in case of error.
	txHash := string(ctx.QueryArgs().Peek("txHash"))
	_, err = chainhash.NewHashFromStr(txHash)
	if err != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidHash)
		return
	}

	txInIdxStr := string(ctx.QueryArgs().Peek("txInputIndex"))
	txInIdx, reportedError = strconv.ParseInt(txInIdxStr, 10, 64)
	if err != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidTxInputIdx)
		return
	}

	//url := fmt.Sprintf("%s/shards/%d/indexes/addresses/transactions/with-input/?address=%s&tx_input=%s&tx_input_idx=%d",
	//config.Config.Explorer.HostURL(), shardID, address.String(), txHash, txInIdx)

	url := fmt.Sprintf("%s/api/v1/shards/%d/indexes/utxos/by-hash/?hash=%s",
		config.Config.Explorer.HostURL(), shardID, txHash)
	bodyBytes, err := httpx.DoGetRequest(url)
	if err != nil {
		return
	}

	var result = struct {
		Data []utxoResponse `json:"data"`
	}{}
	err = json.Unmarshal(bodyBytes, &result)
	if err != nil {
		return
	}

	var isUsed bool
	for _, el := range result.Data {
		if el.Output == int(txInIdx) {
			isUsed = el.Spent
		}
	}

	response := &httpx.Response{Data: InputTxUsingStatus{IsUsed: isUsed}}
	err = httpx.WriteJSONResponse(ctx, response)
}

type utxoResponse struct {
	Hash       string `json:"hash"`
	Output     int    `json:"output"`
	Amount     int    `json:"amount"`
	PkScript   string `json:"pkScript"`
	Spent      bool   `json:"spent"`
	Locked     bool   `json:"locked"`
	LockPeriod int    `json:"lock_period"`
}
