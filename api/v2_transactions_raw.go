package api

import (
	"errors"
	"fmt"

	"github.com/valyala/fasthttp"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/btcapi"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/httpx"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
)

func V2TransactionsRaw(ctx *fasthttp.RequestCtx) {
	var (
		err, reportedError error
		shardID            uint32
		hashes             []string
	)
	defer httpx.HandleErrors(&err, &reportedError, ctx)

	shardID, reportedError = httpx.GetShardIDFromPath(ctx)
	if reportedError != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidShardID)
		return
	}

	// todo: add support of JAX/JXN txs publishing when mempool sync would be ready.
	if shardID != ec.BTCShardIndex {
		reportedError = fmt.Errorf("only BTC is supported by this method for now")
		ec.AssignErrorCode(&reportedError, errCodeInvalidShardID)
		return
	}

	hashes, reportedError = httpx.GetHashesHexListFromParams(ctx)
	if reportedError != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidHash)
		return
	}

	responseData := make(map[string]string)
	for _, h := range hashes {
		var (
			url        = "/transactions/" + h + "/raw-data"
			statusCode int
		)

		item, statusCode, err := btcapi.GetOneItem("blockchain-data", url)
		if statusCode == fasthttp.StatusNotFound {
			// TX not found snd should not be included into the response.
			continue
		}

		if err != nil {
			ec.AssignErrorAndHttpCode(&reportedError, errCodeServerError, fasthttp.StatusInternalServerError)
			return
		}

		data, isPresent := item["transactionHex"]
		if !isPresent {
			_ = ec.EmitError("unexpected response from the BTC API, no transactionHex in returned data: %+v", item)
			ec.AssignErrorAndHttpCode(&reportedError, errCodeServerError, fasthttp.StatusInternalServerError)
			return
		}

		hex, ok := data.(string)
		if !ok {
			_ = ec.EmitError("unexpected response from the BTC API, no transactionHex in returned data: %+v", item)
			ec.AssignErrorAndHttpCode(&reportedError, errCodeServerError, fasthttp.StatusInternalServerError)
			return
		}

		responseData[h] = hex
	}

	if len(hashes) == 0 {
		reportedError = errors.New("there are no hashes")
		ec.AssignErrorCode(&reportedError, errCodeInvalidHash)
		return
	}

	response := &httpx.Response{}
	response.Data = responseData
	err = httpx.WriteJSONResponse(ctx, response)
	return
}
