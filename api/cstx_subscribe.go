package api

import (
	"encoding/json"
	"errors"
	"net/url"

	"github.com/google/uuid"
	"github.com/valyala/fasthttp"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/httpx"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
	"gitlab.com/peace-wallet/back-end/proxy-back/subscriptions/subscription"
)

type SubscribingTxRequest struct {
	UUID                           string `json:"uuid"`
	EAD                            string `json:"ead"`
	Token                          string `json:"token"`
	SourceShardID                  uint32 `json:"sourceShardId"`
	DestinationShardId             uint32 `json:"destinationShardId"`
	SourceShardMaxBlockHeight      int64  `json:"sourceShardMaxBlockHeight"`
	DestinationShardMaxBlockHeight int64  `json:"destinationShardMaxBlockHeight"`
}

func CrossShardTxSubscribe(ctx *fasthttp.RequestCtx) {

	var (
		err           error
		reportedError error
		data          = SubscribingTxRequest{}
	)
	defer httpx.HandleErrors(&err, &reportedError, ctx)

	err = json.Unmarshal(ctx.Request.Body(), &data)
	if err != nil {
		reportedError = errors.New("invalid request body")
		ec.AssignErrorCode(&reportedError, errCodeInvalidRequestBody)
		return
	}

	txUUID, err := validateUUID(data.UUID)
	if err != nil {
		reportedError = errors.New("invalid uuid")
		ec.AssignErrorCode(&reportedError, errCodeInvalidTxUUID)
		return
	}

	if !validateEAD(data.EAD) {
		reportedError = errors.New("invalid ead")
		ec.AssignErrorCode(&reportedError, errCodeInvalidEAD)
		return
	}

	if data.Token == "" {
		reportedError = errors.New("invalid token")
		ec.AssignErrorCode(&reportedError, errCodeInvalidToken)
		return
	}

	err = subscription.WriteSubscriptionToDatabase(txUUID, data.EAD, data.Token, data.SourceShardID,
		data.DestinationShardId, data.SourceShardMaxBlockHeight, data.DestinationShardMaxBlockHeight)
}

func validateUUID(uuidReq string) (txUUID uuid.UUID, err error) {
	txUUID, err = uuid.Parse(uuidReq)
	return
}

func validateEAD(eadReq string) (valid bool) {
	_, err := url.Parse(eadReq)
	valid = err == nil
	return
}
