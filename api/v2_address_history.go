package api

import (
	"encoding/json"
	"fmt"
	"strconv"

	"gitlab.com/peace-wallet/back-end/proxy-back/config"

	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/btcapi"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/httpx"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
	"gitlab.com/peace-wallet/back-end/proxy-back/handlers/current_blocks"
)

type TxRecord struct {
	ID             string        `json:"id"`   // TxHash
	Hash           string        `json:"hash"` // TxWitnessHash
	BlockNumber    int64         `json:"blockNumber"`
	CrossShardTx   bool          `json:"crossShardTx"`
	PairedShardID  uint32        `json:"pairedShardID,omitempty"` // present only if cross-shard.
	Timestamp      int64         `json:"timestamp"`
	Confirmations  int64         `json:"confirmations"`  // 0 for mempool
	Amount         int64         `json:"amount"`         // Balance delta by the caller's address perspective.
	SentAmount     int64         `json:"sentAmount"`     // Sent amount by the caller's address perspective.
	ReceivedAmount int64         `json:"receivedAmount"` // Received amount by the caller's address perspective.
	Fee            int64         `json:"fee"`            // Transaction fee
	TotalIn        int64         `json:"totalIn"`
	TotalOut       int64         `json:"totalOut"`
	Senders        []TxOperation `json:"senders"`
	Receivers      []TxOperation `json:"receivers"`
}

type TxOperation struct {
	Index  int   `json:"index"`
	Amount int64 `json:"amount"`
	// Address represents the address to/from which the funds have been moved.
	// Note that this address could in many cases will be different from the address,
	// which fetches the history, because translations could move funds
	// for more than one address in the same time.
	Address string `json:"address"`
}

const (
	stateConfirmed   = "confirmed"
	stateUnconfirmed = "unconfirmed"
)

// V2AddressHistory returns transactions history for the address passed in arguments.
// Returns only confirmed or only unconfirmed TXs (depending on parameter `state`), but not both at the same time.
// To do both is impossible du to the BTC API limitation, which separates confirmed and unconfirmed TX and
// has offset:limit mechanics on both of them.
// To combine confirmed and confirmed TXs on back-end and still to be able to provide offset:limit mechanics,
// special kind of cursors implementation is required, which is not supported by current front-end implementation.
func V2AddressHistory(ctx *fasthttp.RequestCtx) {
	const (
		maxLimit  = 50
		minLimit  = 1
		minOffset = 0
	)

	var (
		err           error
		reportedError error
		address       jaxutil.Address
		limit         int64
		limitRaw      = string(ctx.QueryArgs().Peek("limit"))
		offset        int64
		offsetRaw     = string(ctx.QueryArgs().Peek("offset"))
		shard         int64
		shardRaw      = string(ctx.QueryArgs().Peek("shard"))
		state         = string(ctx.QueryArgs().Peek("state"))
	)
	defer httpx.HandleErrors(&err, &reportedError, ctx)

	{
		if limitRaw == "" {
			limitRaw = fmt.Sprint(maxLimit)
		}

		limit, err = strconv.ParseInt(limitRaw, 10, 64)
		if err != nil || limit > maxLimit || limit < minLimit {
			reportedError = fmt.Errorf("invalid argument `limit`, must be integer %d <= limit <= %d", minLimit, maxLimit)
			ec.AssignErrorCode(&reportedError, errCodeInvalidLimit)
			return
		}
	}

	{
		if offsetRaw == "" {
			offsetRaw = fmt.Sprint(minOffset)
		}

		offset, err = strconv.ParseInt(offsetRaw, 10, 64)
		if err != nil || offset < minOffset {
			reportedError = fmt.Errorf("invalid argument `offset`, must be integer >= %d", minOffset)
			ec.AssignErrorCode(&reportedError, errCodeInvalidOffset)
			return
		}
	}

	{
		shard, err = strconv.ParseInt(shardRaw, 10, 64)
		if err != nil || shard < 0 || shard > int64(ec.BTCShardIndex) {
			reportedError = fmt.Errorf("invalid argument `shard`, must be integer 0 <= shard <= %d", ec.BTCShardIndex)
			ec.AssignErrorCode(&reportedError, errCodeInvalidShardID)
			return
		}
	}

	{
		address, reportedError = httpx.GetJaxAddressFromPath(ctx)
		if reportedError != nil {
			ec.AssignErrorCode(&reportedError, errCodeInvalidAddress)
			return
		}
	}

	{
		if state == "" {
			state = stateConfirmed
		} else {
			if state != stateConfirmed && state != stateUnconfirmed {
				reportedError = fmt.Errorf("invalid argument `state`, possible options are `confirmed` and `unconfirmed`")
				ec.AssignErrorCode(&reportedError, errCodeInvalidState)
				return
			}
		}
	}

	var records []TxRecord
	if shard == int64(ec.BTCShardIndex) {
		records, err = fetchBTCHistoryRecords(address.EncodeAddress(), state, limit, offset)
		if err != nil {
			return
		}
	} else {
		records, err = fetchJAXHistoryRecords(address.String(), limit, offset, uint32(shard))
		if err != nil {
			return
		}
	}

	enrichTXRecordsWithContextDependantInfo(records, address.String())
	err = httpx.WriteJSONResponse(ctx, &httpx.Response{Data: records})
}

func fetchBTCHistoryRecords(address, state string, limit, offset int64) (records []TxRecord, err error) {
	var urlPart string
	switch state {
	case stateConfirmed:
		urlPart = fmt.Sprintf("addresses/%s/transactions?offset=%d&limit=%d", address, offset, limit)

	case stateUnconfirmed:
		// ya, superior designed API :(
		// especially the namespace
		urlPart = fmt.Sprintf(
			"address-transactions-unconfirmed/%s?offset=%d&limit=%d",
			address, offset, limit)

	default:
		err = ec.EmitError("unexpected argument `state`: available options are %s and %s, value=(%s)",
			state, stateConfirmed, stateUnconfirmed)
		return
	}

	var (
		results               []interface{}
		currentBTCBlockHeight int64
	)
	results, _, err = btcapi.GetListOfItems("blockchain-data", urlPart)
	if err != nil {
		err = fmt.Errorf("BTC API call failed: %w", err)
		return
	}

	// Getting a current BTC block api is a costly operation:
	// It uses the BTC API call under the hood and caches the result for some time,
	// but concurrent access is handled through the mutex,
	// so it is better to fetch this number once and to use it for all records in the response.
	currentBTCBlockHeight, err = current_blocks.GetLastBtcBlock()
	if err != nil {
		err = fmt.Errorf("cant fetch current BTC block: %w", err)
		return
	}

	records = make([]TxRecord, 0, limit)
	for _, result := range results {
		data, ok := result.(map[string]interface{})
		if !ok {
			err = ec.EmitError("can't convert interface to map, API response: %+v", records)
			return
		}

		minedInBlockHeightRaw, minedInBlockHeightIsPresent := data["minedInBlockHeight"]
		timestampRaw, timestampIsPresent := data["timestamp"]
		hashRaw, hashIsPresent := data["transactionId"]
		sendersRaw, sendersArePresent := data["senders"]
		receiversRaw, receiversArePresent := data["recipients"]

		if !minedInBlockHeightIsPresent || !timestampIsPresent || !hashIsPresent || !sendersArePresent || !receiversArePresent {
			err = ec.EmitError("one or several required fields are missed in API response: %+v", records)
			return
		}

		minedInBlockHeightFloat, blockHeightConverted := minedInBlockHeightRaw.(float64)
		timestamp, timestampConverted := timestampRaw.(float64)
		hash, hashConverted := hashRaw.(string)
		senders, sendersConverted := sendersRaw.([]interface{})
		receivers, receiversConverted := receiversRaw.([]interface{})

		if !blockHeightConverted || !timestampConverted || !hashConverted || !sendersConverted || !receiversConverted {
			err = fmt.Errorf("invalid BTC API response: one of required field could not be converted to the expected type")
			return
		}

		tx := TxRecord{
			Confirmations: currentBTCBlockHeight - int64(minedInBlockHeightFloat),
			Timestamp:     int64(timestamp),
			Hash:          hash,
			Senders:       make([]TxOperation, 0, 2),
			Receivers:     make([]TxOperation, 0, 2),
		}

		for _, senderRaw := range senders {
			sender, ok := senderRaw.(map[string]interface{})
			if !ok {
				err = ec.EmitError("can't decode senders list, response: %+v", records)
				return
			}

			addressRaw, addressIsPresent := sender["address"]
			amountRaw, amountIsPresent := sender["amount"]
			if !addressIsPresent || !amountIsPresent {
				err = ec.EmitError("can't decode address or amount of sender, response: %+v, sender: %+v", records, sender)
				return
			}

			address, addressConverted := addressRaw.(string)
			amountStr, amountConverted := amountRaw.(string)
			if !addressConverted || !amountConverted {
				err = ec.EmitError("can't decode address or amount of sender, response: %+v, sender: %+v", records, sender)
				return
			}

			var amountFloat float64
			amountFloat, err = strconv.ParseFloat(amountStr, 64)
			if err != nil {
				err = ec.EmitError("can't decode amount of sender, response: %+v, sender: %+v", records, sender)
				return
			}

			operation := TxOperation{
				Amount:  int64(amountFloat * 100000000),
				Address: address,
			}

			tx.Senders = append(tx.Senders, operation)
		}

		for _, receiverRaw := range receivers {
			receiver, ok := receiverRaw.(map[string]interface{})
			if !ok {
				err = ec.EmitError("can't decode senders list, response: %+v", records)
				return
			}

			addressRaw, addressIsPresent := receiver["address"]
			amountRaw, amountIsPresent := receiver["amount"]
			if !addressIsPresent || !amountIsPresent {
				err = ec.EmitError("can't decode address or amount of receiver, response: %+v, receiver: %+v", records, receiver)
				return
			}

			address, addressConverted := addressRaw.(string)
			amountStr, amountConverted := amountRaw.(string)
			if !addressConverted || !amountConverted {
				err = ec.EmitError("can't decode address or amount of receiver, response: %+v, receiver: %+v", records, receiver)
				return
			}

			var amountFloat float64
			amountFloat, err = strconv.ParseFloat(amountStr, 64)
			if err != nil {
				err = ec.EmitError("can't decode amount of sender, response: %+v, sender: %+v", records, receiver)
				return
			}

			operation := TxOperation{
				Amount:  int64(amountFloat * 100000000),
				Address: address,
			}

			tx.Receivers = append(tx.Receivers, operation)
		}

		records = append(records, tx)
	}

	return
}

func fetchJAXHistoryRecords(address string, limit, offset int64, shardID uint32) ([]TxRecord, error) {
	url := fmt.Sprintf("%s/api/v1/shards/%d/addresses/%s/history/v2?limit=%d&offset=%d",
		config.Config.Explorer.HostURL(), shardID, address, limit, offset)

	body, err := httpx.DoGetRequest(url)
	if err != nil {
		return nil, err
	}

	result := struct {
		Data []TxRecord `json:"data"`
	}{}
	err = json.Unmarshal(body, &result)
	return result.Data, err
}

// enrichTXRecordsWithContextDependantInfo calculates the amount of TX
// by `address`'s perspective of view. See `TxRecord` documentation for the details.
func enrichTXRecordsWithContextDependantInfo(records []TxRecord, address string) {
	for _, record := range records {
		var (
			totalSentAmount     int64
			totalReceivedAmount int64
		)

		for _, sender := range record.Senders {
			if sender.Address == address {
				totalSentAmount += sender.Amount
			}
		}

		for _, receiver := range record.Receivers {
			if receiver.Address == address {
				totalReceivedAmount += receiver.Amount
			}
		}

		record.Amount = totalReceivedAmount - totalSentAmount
		record.Fee = totalSentAmount - totalReceivedAmount
	}
}
