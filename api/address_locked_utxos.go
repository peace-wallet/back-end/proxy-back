package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"

	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/httpx"
	"gitlab.com/peace-wallet/back-end/proxy-back/config"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
)

func AddressLockedUTXOs(ctx *fasthttp.RequestCtx) {
	var (
		err           error
		reportedError error
		address       jaxutil.Address
		offset        = string(ctx.QueryArgs().Peek("offset"))
		iOffset       int
		limit         = string(ctx.QueryArgs().Peek("limit"))
		iLimit        int
		shardsIDs     = ctx.QueryArgs().PeekMulti("shard")
	)
	defer httpx.HandleErrors(&err, &reportedError, ctx)

	from, ok := getIntFromQuery(ctx, "from")
	if !ok {
		return
	}
	to, ok := getIntFromQuery(ctx, "from")
	if !ok {
		return
	}

	if (from != 0 && to == 0) || (to != 0 && from == 0) {
		reportedError = errors.New("arguments `from` and `to` must be always specified together")
		ec.AssignErrorCode(&reportedError, errCodeInvalidRequestArguments)
		return
	}

	if (offset == "" && limit != "") || (offset != "" && limit == "") {
		reportedError = errors.New("arguments `offset` and `limit` must be always specified together")
		ec.AssignErrorCode(&reportedError, errCodeInvalidRequestArguments)
		return
	}

	if offset == "" && limit == "" {
		iOffset = -1
		iLimit = -1
	} else {
		iOffset, err = strconv.Atoi(offset)
		if err != nil {
			reportedError = errors.New("arguments `offset` must be integer value")
			ec.AssignErrorCode(&reportedError, errCodeInvalidRequestArguments)
			return
		}
		iLimit, err = strconv.Atoi(limit)
		if err != nil {
			reportedError = errors.New("arguments `limit` must be integer value")
			ec.AssignErrorCode(&reportedError, errCodeInvalidRequestArguments)
			return
		}
	}

	address, reportedError = httpx.GetJaxAddressFromPath(ctx)
	if reportedError != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidAddress)
		return
	}

	parsedShardIDs, err := parseShards(shardsIDs)
	if err != nil {
		reportedError = errors.New("could not parse shard IDs provided")
		ec.AssignErrorCode(&reportedError, errCodeInvalidRequestArguments)
		return
	}

	err = writeLockedUTXOsRecordsForBothJaxAndBtc(ctx, address.String(), parsedShardIDs, from, to, iOffset, iLimit)
}

type LockedUTXO struct {
	TxHash             string `json:"hash"`
	TxOutput           uint16 `json:"output"`
	Amount             int64  `json:"amount"`
	UnlockingBlock     uint64 `json:"unlockingBlock"`
	Timestamp          int64  `json:"timestamp"`
	UnlockingTimestamp int64  `json:"unlockingTimestamp"`
	ShardID            uint32 `json:"shardId"`

	PkScript   string `json:"pkScript"`
	Spent      bool   `json:"spent"`
	Locked     bool   `json:"locked"`
	LockPeriod int    `json:"lockPeriod"`
	BlockHash  string `json:"blockHash"`
	BlockTime  int    `json:"blockTime"`
}

type StatsResponse struct {
	TotalCount  int   `json:"totalCount"`
	TotalAmount int64 `json:"totalAmount"`
}

type ProxyResponse struct {
	UTXOs        []LockedUTXO              `json:"utxos"`
	Stats        StatsResponse             `json:"stats"`
	NearestDates NearestDatesIndexResponse `json:"nearestDates"`
}

type NearestDatesIndexResponse struct {
	Next     int64 `json:"next"`
	Previous int64 `json:"previous"`
}

type IndexResponse struct {
	LockedUTXOs  []LockedUTXO              `json:"data"`
	NearestDates NearestDatesIndexResponse `json:"nearestDates"`
}

func writeLockedUTXOsRecordsForBothJaxAndBtc(ctx *fasthttp.RequestCtx, address string, shardsIDs []uint32,
	from, to int64, offset, limit int) (err error) {
	var nextDate int64 = -1
	var previousDate int64 = -1
	var totalCount int
	var totalAmount int64

	lockedUTXOsResponse := make([]LockedUTXO, 0)
	for _, shardID := range shardsIDs {
		// BTC does not have locked tokens in terms how it is interpreted by JAX.
		if shardID == ec.BTCShardIndex {
			continue
		}

		var reqURL = fmt.Sprintf("%s/api/v1/shards/%d/indexes/utxos/locked/?address=%s&since=%d&until=%d",
			config.Config.Explorer.HostURL(), shardID, address, from, to)

		var indexData []byte
		indexData, err = httpx.DoGetRequest(reqURL)
		if err != nil {
			return err
		}

		var indexResponse IndexResponse
		err = json.Unmarshal(indexData, &indexResponse)
		if err != nil {
			return err
		}

		for idx, lockedUTXO := range indexResponse.LockedUTXOs {
			// current version of indexer returns this date as a Timestamp
			indexResponse.LockedUTXOs[idx].UnlockingTimestamp = indexResponse.LockedUTXOs[idx].Timestamp
			indexResponse.LockedUTXOs[idx].ShardID = shardID
			totalAmount += lockedUTXO.Amount
		}
		totalCount += len(indexResponse.LockedUTXOs)
		lockedUTXOsResponse = append(lockedUTXOsResponse, indexResponse.LockedUTXOs...)

		if nextDate == -1 {
			nextDate = indexResponse.NearestDates.Next
		} else if indexResponse.NearestDates.Next < nextDate && indexResponse.NearestDates.Next != -1 {
			nextDate = indexResponse.NearestDates.Next
		}

		if indexResponse.NearestDates.Previous > previousDate {
			previousDate = indexResponse.NearestDates.Previous
		}
	}

	if len(shardsIDs) > 1 {
		// sort if more than one shard data exists
		for idx := 0; idx < len(lockedUTXOsResponse)-1; idx++ {
			jdxMin := idx
			for jdx := idx + 1; jdx < len(lockedUTXOsResponse); jdx++ {
				if lockedUTXOsResponse[jdx].Timestamp < lockedUTXOsResponse[idx].Timestamp {
					jdxMin = jdx
				}
			}
			if jdxMin != idx {
				tmp := lockedUTXOsResponse[idx]
				lockedUTXOsResponse[idx] = lockedUTXOsResponse[jdxMin]
				lockedUTXOsResponse[jdxMin] = tmp
			}
		}
	}

	if offset > -1 && limit > -1 {
		if offset >= len(lockedUTXOsResponse) {
			lockedUTXOsResponse = make([]LockedUTXO, 0)
		} else if offset+limit >= len(lockedUTXOsResponse) {
			lockedUTXOsResponse = lockedUTXOsResponse[offset:]
		} else {
			lockedUTXOsResponse = lockedUTXOsResponse[offset : offset+limit]
		}
	}

	proxyResponse := &ProxyResponse{
		UTXOs: lockedUTXOsResponse,
		Stats: StatsResponse{
			TotalCount:  totalCount,
			TotalAmount: totalAmount,
		},
	}

	if nextDate != -1 {
		proxyResponse.NearestDates.Next = nextDate
	}
	if previousDate != -1 {
		proxyResponse.NearestDates.Previous = previousDate
	}

	err = httpx.WriteJSONResponse(ctx, &httpx.Response{Data: proxyResponse})
	return
}
