package api

import (
	"fmt"

	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/httpx"
	"gitlab.com/peace-wallet/back-end/proxy-back/config"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
)

func AddressBalances(ctx *fasthttp.RequestCtx) {
	var (
		err, reportedError error
		address            jaxutil.Address
	)
	defer httpx.HandleErrors(&err, &reportedError, ctx)

	address, reportedError = httpx.GetJaxAddressFromPath(ctx)
	if reportedError != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidAddress)
		return
	}

	url := fmt.Sprintf("%s/api/v1/addresses/%s/balances/", config.Config.Explorer.HostURL(), address.String())
	err = httpx.ProxyGetJSON(ctx, url)
}
