package api

import (
	"fmt"

	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/btcapi"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/httpx"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
	"gitlab.com/peace-wallet/back-end/proxy-back/handlers/current_blocks"
)

func V2TransactionDetails(ctx *fasthttp.RequestCtx) {
	var (
		err, reportedError error
		shardID            uint32
		hash               *chainhash.Hash
		statusCode         int
	)
	defer httpx.HandleErrors(&err, &reportedError, ctx)

	shardID, reportedError = httpx.GetShardIDFromPath(ctx)
	if reportedError != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidShardID)
		return
	}

	if shardID != ec.BTCShardIndex {
		reportedError = fmt.Errorf("only BTC is supported by this method for now")
		ec.AssignErrorCode(&reportedError, errCodeInvalidShardID)
		return
	}

	hash, reportedError = httpx.GetHashFromPath(ctx)
	if reportedError != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidHash)
		return
	}

	// /blockchain-data/{blockchain}/{network}/transactions/{transactionId}
	url := "/transactions/" + hash.String()
	item, statusCode, err := btcapi.GetOneItem("blockchain-data", url)
	switch statusCode {
	case fasthttp.StatusOK:
		err = enrichTxDetailsWithCurrentBlock(item)
		if err != nil {
			ec.AssignErrorAndHttpCode(&reportedError, errCodeServerError, fasthttp.StatusInternalServerError)
			return
		}

		err = httpx.WriteJSONResponse(ctx, &httpx.Response{Data: item})
		return

	case fasthttp.StatusNotFound:
		ctx.SetStatusCode(fasthttp.StatusNotFound)
		ctx.SetBody([]byte(`{"error": "tx not found"}`))
		return

	default:
		ec.AssignErrorAndHttpCode(&reportedError, errCodeServerError, fasthttp.StatusInternalServerError)
		return
	}
}

func enrichTxDetailsWithCurrentBlock(tx map[string]interface{}) (err error) {
	minedInBlockRaw, isPresent := tx["minedInBlockHeight"]
	if !isPresent {
		err = ec.EmitError("'minedInBlockHeight' doesn't present in returned BTC API data: %+v", tx)
		return
	}

	minedInBlockFloat, ok := minedInBlockRaw.(float64)
	if !ok {
		err = ec.EmitError("'minedInBlockHeight' has unexpected format (float64 is expected): %+v", tx)
		return
	}

	minedInBlockInt := int64(minedInBlockFloat)
	currentBTCBlock, err := current_blocks.GetLastBtcBlock()
	if err != nil {
		return
	}

	tx["confirmations"] = currentBTCBlock - minedInBlockInt
	return
}
