package api

import (
	"encoding/json"
	"errors"

	"github.com/valyala/fasthttp"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/httpx"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
)

type RawTxRequest struct {
	Hashes []string `json:"hashes"`
}

func TransactionsRaw(ctx *fasthttp.RequestCtx) {
	var (
		err, reportedError error
		data               = RawTxRequest{}
		shardID            uint32
	)
	defer httpx.HandleErrors(&err, &reportedError, ctx)

	shardID, reportedError = httpx.GetShardIDFromPath(ctx)
	if reportedError != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidShardID)
		return
	}

	if shardID == ec.BTCShardIndex {
		ec.AssignErrorCode(&reportedError, errCodeInvalidShardID)
		return
	}

	err = json.Unmarshal(ctx.Request.Body(), &data)
	if err != nil {
		reportedError = errors.New("invalid request body")
		ec.AssignErrorCode(&reportedError, errCodeInvalidRequestBody)
		return
	}

	if len(data.Hashes) == 0 {
		reportedError = errors.New("there are no hashes")
		ec.AssignErrorCode(&reportedError, errCodeInvalidHash)
		return
	}

	reportedError, err = writeJaxResponse(ctx, shardID, data.Hashes)
}
