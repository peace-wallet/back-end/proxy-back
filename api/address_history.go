package api

import (
	"errors"
	"fmt"
	"sort"
	"strconv"

	jsoniter "github.com/json-iterator/go"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/peace-wallet/back-end/proxy-back/api/common/httpx"
	"gitlab.com/peace-wallet/back-end/proxy-back/config"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
	"gitlab.com/peace-wallet/back-end/proxy-back/rpc"
)

// AddressHistory uses `from:to` request semantic,
// which is not supported by BTC API and would not be supported by the next gen indexes API.
// Use V2AddressHistory instead.
//
// Deprecated
func AddressHistory(ctx *fasthttp.RequestCtx) {
	var (
		err           error
		reportedError error
		address       jaxutil.Address
		shardsIDs     = ctx.QueryArgs().PeekMulti("shard")
	)

	defer httpx.HandleErrors(&err, &reportedError, ctx)

	from, ok := getIntFromQuery(ctx, "from")
	if !ok {
		return
	}
	to, ok := getIntFromQuery(ctx, "to")
	if !ok {
		return
	}

	if (from != 0 && to == 0) || (to != 0 && from == 0) {
		reportedError = errors.New("arguments `from` and `to` must be always specified together")
		ec.AssignErrorCode(&reportedError, errCodeInvalidRequestArguments)
		return
	}

	address, reportedError = httpx.GetJaxAddressFromPath(ctx)
	if reportedError != nil {
		ec.AssignErrorCode(&reportedError, errCodeInvalidAddress)
		return
	}

	parsedShardIDs, err := parseShards(shardsIDs)
	if err != nil {
		reportedError = errors.New("could not parse shard IDs provided")
		ec.AssignErrorCode(&reportedError, errCodeInvalidRequestArguments)
		return
	}

	err = aggregateHistory(ctx, address.String(), parsedShardIDs, from, to)
}

func aggregateHistory(ctx *fasthttp.RequestCtx, address string, shardsIDs []uint32, from, to int64) error {
	result := HistoryResponse{
		ShardsHeight: map[uint32]int64{},
	}

	nearestDates := make([]NearestDatesIndexResponse, 0, len(shardsIDs))

	for _, shardID := range shardsIDs {
		client, err := rpc.AcquireJAXClient(shardID)
		if err != nil {
			return err
		}

		count, err := client.GetBlockCount(shardID)
		if err != nil {
			return err
		}

		result.ShardsHeight[shardID] = count

		reqURL := fmt.Sprintf("%s/api/v1/shards/%d/addresses/%s/history/?since=%d&until=%d",
			config.Config.Explorer.HostURL(), shardID, address, from, to)
		historyRespRaw, err := httpx.DoGetRequest(reqURL)
		if err != nil {
			return err
		}

		response := struct {
			Records      []TxRecord                `json:"data"`
			NearestDates NearestDatesIndexResponse `json:"nearestDates"`
		}{}

		if err = jsoniter.Unmarshal(historyRespRaw, &response); err != nil {
			return err
		}

		records := convertTxRecordsToHistoryItems(shardID, address, response.Records)
		result.History.Records = append(result.History.Records, records...)
		result.History.NearestDates = response.NearestDates
		nearestDates = append(nearestDates, response.NearestDates)
	}

	for _, date := range nearestDates {
		if (result.History.NearestDates.Previous == 0) ||
			(date.Previous > result.History.NearestDates.Previous) {
			result.History.NearestDates.Previous = date.Previous
		}

		if (result.History.NearestDates.Next == 0) ||
			(date.Next < result.History.NearestDates.Next) {
			result.History.NearestDates.Next = date.Next
		}
	}

	sort.Slice(result.History.Records, func(i, j int) bool {
		return result.History.Records[i].Timestamp < result.History.Records[j].Timestamp
	})

	err := httpx.WriteJSONResponse(ctx, &httpx.Response{Data: result})
	return err
}

type HistoryResponse struct {
	ShardsHeight map[uint32]int64 `json:"shardsHeight"`
	History      History          `json:"history"`
}

type History struct {
	Records      []HistoryItem             `json:"records"`
	NearestDates NearestDatesIndexResponse `json:"nearestDates"`
}

type HistoryItem struct {
	ShardID     uint32   `json:"shardId"`
	BlockNumber int64    `json:"blockNumber"`
	TxHash      string   `json:"hash"`
	Amount      int64    `json:"amount"`
	TxFee       int64    `json:"txFee"`
	Direction   string   `json:"direction"`
	Addresses   []string `json:"addresses"`
	Timestamp   int64    `json:"timestamp"`
}

func parseShards(ids [][]byte) (shardIDs []uint32, err error) {
	var parsedShard uint64

	for _, shardID := range ids {
		parsedShard, err = strconv.ParseUint(string(shardID), 10, 32)
		if err != nil {
			err = fmt.Errorf("can't parse shardID: %w", err)
			return
		}

		shardIDs = append(shardIDs, uint32(parsedShard))
	}

	return
}

func getIntFromQuery(ctx *fasthttp.RequestCtx, name string) (int64, bool) {
	fromStr := string(ctx.QueryArgs().Peek(name))
	from, err := strconv.ParseInt(fromStr, 10, 64)
	if err != nil {
		reportedError := fmt.Errorf("arguments `%s` must be valid integer", name)
		ec.AssignErrorCode(&reportedError, errCodeInvalidRequestArguments)
		httpx.HandleErrors(&err, &reportedError, ctx)
		return 0, false
	}

	return from, true
}

func convertTxRecordsToHistoryItems(shardID uint32, address string, records []TxRecord) []HistoryItem {
	result := make([]HistoryItem, 0, len(records))
	for _, record := range records {
		addressSet := map[string]struct{}{}
		var direction string
		for _, sender := range record.Senders {
			addressSet[sender.Address] = struct{}{}
			if sender.Address == address {
				direction = "out"
			}
		}
		for _, receiver := range record.Receivers {
			addressSet[receiver.Address] = struct{}{}
			if receiver.Address == address && direction == "" {
				direction = "in"
			}
		}

		addresses := make([]string, 0, len(addressSet))
		for a := range addressSet {
			addresses = append(addresses, a)
		}

		result = append(result, HistoryItem{
			ShardID:     shardID,
			BlockNumber: record.BlockNumber,
			TxHash:      record.Hash,
			Amount:      record.Amount,
			TxFee:       record.Fee,
			Direction:   direction,
			Addresses:   addresses,
			Timestamp:   record.Timestamp,
		})
	}

	return result
}
