package processing

import (
	"time"

	"gitlab.com/peace-wallet/back-end/proxy-back/config"
	"gitlab.com/peace-wallet/back-end/proxy-back/logger"
	"gitlab.com/peace-wallet/back-end/proxy-back/subscriptions/subscription"
)

func RunSubscriptionDelivering() {
	time.Sleep(time.Second * 5)

	for {
		subscriptions, err := subscription.GetAllSubscriptionsByStatus(subscription.Confirmed)
		if err != nil {
			postponeStateDelivering(err)
		}

		for _, s := range subscriptions {
			err := deliverPushOk(s)
			if err != nil {
				logger.Log.Err(err).Msg("push delivering")
				continue
			}

			err = subscription.UpdateSubscriptionStatus(s, subscription.Delivered)
			if err != nil {
				logger.Log.Err(err).Msg("can't update subscription status")
			}
		}

		subscriptions, err = subscription.GetAllSubscriptionsByStatus(subscription.Faulty)
		if err != nil {
			postponeStateDelivering(err)
		}

		for _, s := range subscriptions {
			err := deliverPushError(s)
			if err != nil {
				logger.Log.Err(err).Msg("push delivering")
				continue
			}

			err = subscription.UpdateSubscriptionStatus(s, subscription.Delivered)
			if err != nil {
				logger.Log.Err(err).Msg("can't update subscription status")
			}
		}

		postponeStateDelivering(nil)
	}
}

func deliverPushOk(subscription *subscription.Subscription) (err error) {
	logger.Log.Debug().Uint64("id", subscription.ID).Msg("ok push delivered")
	return
}

func deliverPushError(subscription *subscription.Subscription) (err error) {
	logger.Log.Debug().Uint64("id", subscription.ID).Msg("error push delivered")
	return
}

func postponeStateDelivering(err error) {
	if err != nil {
		logger.Log.Err(err).Msg("deliver push processing")
	}
	time.Sleep(time.Second * time.Duration(config.Config.CSTXProcessing.DeliverCSTXStateToClientPeriodInSec))
}
