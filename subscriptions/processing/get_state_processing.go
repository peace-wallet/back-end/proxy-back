package processing

import (
	"errors"
	"fmt"
	"time"

	jsoniter "github.com/json-iterator/go"
	"github.com/valyala/fasthttp"
	"gitlab.com/peace-wallet/back-end/proxy-back/config"
	"gitlab.com/peace-wallet/back-end/proxy-back/logger"
	"gitlab.com/peace-wallet/back-end/proxy-back/subscriptions/subscription"
)

// the same statuses as on EAD side
const (
	OK                         = 0
	TransactionAborted         = 1
	InProgress                 = 2
	Completed                  = 3
	NoCorrespondingTransaction = 254
	Error                      = 255
)

func RunSubscriptionProcessing() {
	time.Sleep(time.Second * 5)

	for {
		subscriptions, err := subscription.GetAllSubscriptionsByStatus(subscription.Initialised)
		if err != nil {
			postponeGetStateProcessing(err)
		}

		for _, s := range subscriptions {
			if isSubscriptionOutdated(s) {
				err = subscription.UpdateSubscriptionStatus(s, subscription.Faulty)
				if err != nil {
					logger.Log.Err(err).Msg("can't update subscription status")
				}
				continue
			}

			status, err := getCSTXStatusFromEAD(s)
			if err != nil {
				logger.Log.Err(err).Uint64("id", s.ID).Msg("Get status from EAD")
				continue
			}
			if status == Error {
				logger.Log.Debug().Msg("error tx code in response from ead")
			} else if status == OK {
				err = subscription.UpdateSubscriptionStatus(s, subscription.Confirmed)
				if err != nil {
					logger.Log.Err(err).Msg("can't update subscription status")
				}
			} else if status == NoCorrespondingTransaction || status == TransactionAborted {
				err = subscription.UpdateSubscriptionStatus(s, subscription.Faulty)
				if err != nil {
					logger.Log.Err(err).Msg("can't update subscription status")
				}
			} else if status == Completed {
				err = subscription.UpdateSubscriptionStatus(s, subscription.Delivered)
				if err != nil {
					logger.Log.Err(err).Msg("can't update subscription status")
				}
			}
		}

		postponeGetStateProcessing(nil)
	}
}

func isSubscriptionOutdated(subscription *subscription.Subscription) (isOutdated bool) {
	// sourceShardBlockHeight := current_blocks.CurrentBlockHandler.BlockCount(subscription.SourceShard)
	// destinationShardBlockHeight := current_blocks.CurrentBlockHandler.BlockCount(subscription.DestinationShard)
	// isOutdated = sourceShardBlockHeight > subscription.SourceShardMaxBlockHeight &&
	// destinationShardBlockHeight > subscription.DestinationShardMaxBlockHeight
	return
}

type EADResponse struct {
	Code uint8 `json:"code"`
}

type EADResponseData struct {
	Data EADResponse `json:"data"`
}

func getCSTXStatusFromEAD(subscription *subscription.Subscription) (status uint8, err error) {
	url := fmt.Sprintf("https://%s/api/v1/exchange/cross-shard/?uuid=%s", subscription.EAD, subscription.UUID.String())
	req := fasthttp.AcquireRequest()
	req.SetRequestURI(url)
	req.Header.SetMethod("GET")

	resp := fasthttp.AcquireResponse()
	client := &fasthttp.Client{}
	err = client.Do(req, resp)
	if err != nil {
		return
	}

	if resp.StatusCode() != 200 {
		err = errors.New(fmt.Sprintf("invalid status code (%d) response from ead", resp.StatusCode()))
		return
	}

	var eadResponse EADResponseData
	err = jsoniter.Unmarshal(resp.Body(), &eadResponse)
	if err != nil {
		return
	}

	logger.Log.Debug().Uint8("state", eadResponse.Data.Code).Msg("")
	status = eadResponse.Data.Code
	return
}

func postponeGetStateProcessing(err error) {
	if err != nil {
		logger.Log.Err(err).Msg("get state processing")
	}
	time.Sleep(time.Second * time.Duration(config.Config.CSTXProcessing.GetCSTXStateFromEADPeriodInSec))
}
