package subscription

import (
	"errors"

	"github.com/gofrs/uuid"
)

const (
	Initialised = 0
	Confirmed   = 1
	Faulty      = 2
	Delivered   = 3
	Outdated    = 4
	Error       = 255
)

var (
	ErrInvalidTxState = errors.New("invalid tx state")
	ErrInvalidTxData  = errors.New("invalid tx data")
)

type Subscription struct {
	ID                             uint64
	UUID                           uuid.UUID
	EAD                            string
	SourceShard                    uint32
	SourceShardMaxBlockHeight      int64
	DestinationShard               uint32
	DestinationShardMaxBlockHeight int64
	Token                          string
	Status                         uint8
}

func FromDBData(id int64, binaryUUID [16]byte, ead string, sourceShardID, destShardID uint32,
	sourceShardMaxBlockHeight, destShardMaxBlockHeight int64, token string, status int16) (subscription *Subscription, err error) {

	subscription = &Subscription{}
	subscription.ID, err = ParseAndValidateID(id)
	if err != nil {
		return
	}

	subscription.UUID, err = ParseAndValidateUUID(binaryUUID)
	if err != nil {
		return
	}

	subscription.EAD = ead

	subscription.SourceShard = sourceShardID
	subscription.DestinationShard = destShardID
	subscription.SourceShardMaxBlockHeight = sourceShardMaxBlockHeight
	subscription.DestinationShardMaxBlockHeight = destShardMaxBlockHeight

	subscription.Token = token

	subscription.Status, err = ParseAndValidateStatus(status)
	if err != nil {
		return
	}

	return
}

func ParseAndValidateID(data int64) (id uint64, err error) {
	if data < 0 {
		err = ErrInvalidTxData
	}

	id = uint64(data)
	return
}

func ParseAndValidateUUID(binary [16]byte) (uid uuid.UUID, err error) {
	err = uid.UnmarshalBinary(binary[:])
	if err != nil {
		err = ErrInvalidTxData
		return
	}

	return
}

func ParseAndValidateStatus(status int16) (statusCode uint8, err error) {
	statusCode = uint8(status)
	return
}
