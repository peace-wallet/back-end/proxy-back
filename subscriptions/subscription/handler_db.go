package subscription

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/peace-wallet/back-end/proxy-back/database"
)

func WriteSubscriptionToDatabase(txUUID uuid.UUID, ead, token string, sourceShardId, destShardId uint32, sourceShardMaxBlock, destShardMaxBlock int64) (err error) {
	tx, err := database.DB.Begin(context.Background())
	if err != nil {
		return
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer tx.Rollback(context.Background())

	row := tx.QueryRow(context.Background(),
		"INSERT INTO subscriptions (uuid, ead, token, source_shard_id, destination_shard_id, "+
			"source_shard_max_block_height, destination_shard_max_block_height, status) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id",
		txUUID.String(), ead, token, sourceShardId, destShardId, sourceShardMaxBlock, destShardMaxBlock, Initialised)

	var recordID uint64
	err = row.Scan(&recordID)
	if err != nil {
		err = fmt.Errorf("can't write new subscription to the database: %w", err)
		return
	}

	err = tx.Commit(context.Background())
	return
}

func GetAllSubscriptionsByStatus(status uint8) (subscriptions []*Subscription, err error) {
	query := fmt.Sprintf(
		"SELECT id, uuid, ead, source_shard_id, destination_shard_id, source_shard_max_block_height, "+
			"destination_shard_max_block_height, token, status FROM subscriptions "+
			"WHERE status = %v ORDER BY id", status)

	rows, err := database.DB.Query(context.Background(), query)
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		values, err := rows.Values()
		if err != nil {
			return nil, err
		}

		subscription, err := FromDBData(
			values[0].(int64),
			values[1].([16]byte),
			values[2].(string),
			uint32(values[3].(int64)),
			uint32(values[4].(int64)),
			values[5].(int64),
			values[6].(int64),
			values[7].(string),
			values[8].(int16))

		if err != nil {
			return nil, err
		}

		subscriptions = append(subscriptions, subscription)
	}
	return
}

func UpdateSubscriptionStatus(subscription *Subscription, status uint8) (err error) {
	tx, err := database.DB.Begin(context.Background())
	if err != nil {
		return
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer tx.Rollback(context.Background())

	_, err = tx.Exec(context.Background(),
		"UPDATE subscriptions SET status = $1 where id = $2",
		status, subscription.ID)
	if err != nil {
		return
	}

	err = tx.Commit(context.Background())
	if err != nil {
	}
	subscription.Status = status
	return
}
