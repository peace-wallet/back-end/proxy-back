package ec

import (
	"errors"
	"fmt"
	"math/rand"
	"runtime"
	"strings"

	"github.com/valyala/fasthttp"
	"gitlab.com/peace-wallet/back-end/proxy-back/logger"
)

var (
	ErrConfigValidation = errors.New("configuration validation error")

	ErrMetricIsUndefined = errors.New("metric is undefined")
	ErrDBSchema          = errors.New("invalid DB schema")
)

func EmitError(format string, a ...any) (err error) {
	if strings.Contains(format, "%w") {
		panic("Please, don't encapsulate errors inside of rayed error. " +
			"Otherwise it would lead to duplicated log records for same errors. " +
			"Error must be rayed from the pont where it has occurred, and wrapped/passed further with fmt.Errorf.")
	}

	rayID := rand.Uint64()
	callers := getStackCallers()

	err = fmt.Errorf(format, a...)
	logger.Log.Error().Err(err).Strs("stack-trace", callers).Uint64("ray-id", rayID).Msg("")
	return
}

// HttpErr is helpful in cases when some additional context must be attached
// to an error before it would be returned to the caller.
type HttpErr struct {
	Code           int
	HttpStatusCode int
	Err            error
}

func (e HttpErr) Error() string {
	return fmt.Sprintf("(%d): %s", e.Code, e.Err.Error())
}

// MarshalJSON marshals HttpErr into the json.
// This method overrides standard behaviour of the json marshaller to process the error.
// By default, it is a struct with an unexported string field, so it marshals into an empty structs.
// This method forces it to be marshalled into the string with the error description.
func (e HttpErr) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("{\"code\":%d,\"message\":\"%s\"}", e.Code, e.Err.Error())), nil
}

// AssignErrorCode converts raw error into HttpErr *in place*.
// By default, uses StatusBadRequest as a http code (most useful one).
func AssignErrorCode(err *error, code int) {
	*err = &HttpErr{
		Code:           code,
		Err:            *err,
		HttpStatusCode: fasthttp.StatusBadRequest,
	}
}

// AssignErrorAndHttpCode converts raw error into HttpErr *in place*.
// Allows to specify not only the code of the error,
// but also to provide the hint about which HTTP code must be returned to the user with this particular error.
func AssignErrorAndHttpCode(err *error, code, httpCode int) {
	*err = &HttpErr{
		Code:           code,
		Err:            *err,
		HttpStatusCode: httpCode,
	}
}

func getStackCallers() (callers []string) {
	programCounters := make([]uintptr, 256)
	n := runtime.Callers(0, programCounters)

	frames := runtime.CallersFrames(programCounters[:n])
	callers = make([]string, 0, n-4)
	_, _ = frames.Next() // skip caller itself
	_, _ = frames.Next() // skip getStackCallers()
	_, _ = frames.Next() // skip EmitError()

	for {
		frame, ok := frames.Next()
		if !ok || frame.Function == "runtime.main" {
			break
		}

		s := fmt.Sprintf("%s:%d", frame.Function, frame.Line)
		callers = append(callers, s)
	}

	return
}
