package ec

import (
	"math"
)

const (
	MetricHTTPRequest    = "http_requests"
	MetricLastAction     = "last_action"
	MetricProcessingTime = "processing_time"
)

// BTCShardIndex is used in sync.Map access logic which is type-specific.
// In case if this cosnt would be of another type - this logic breaks.
// For the sync.Map access uin32(0) != int(0).
const BTCShardIndex uint32 = math.MaxUint32

const BeaconShardIndex uint32 = 0
