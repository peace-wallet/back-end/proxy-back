package utils

import (
	"github.com/btcsuite/btcd/blockchain"
	"github.com/btcsuite/btcd/mempool"
	"github.com/btcsuite/btcd/wire"
	"gitlab.com/jaxnet/jaxnetd/jaxutil/txutils"
	"gitlab.com/peace-wallet/back-end/proxy-back/ec"
)

var (
	TxInEstWeight  = 180 // size of regular signed utxo
	TxOutEstWeight = 34  // simple output
	// RegularTxEstWeight = 1040
	// SwapTxEstWeight    = 1800
)

func EstimateFee(inCount, outCount int, feeRate int64, addChange bool, shardID uint32) int64 {
	if shardID == ec.BTCShardIndex {
		return estimateBtcFee(inCount, outCount, feeRate, addChange)
	} else {
		return txutils.EstimateFee(inCount, outCount, feeRate, addChange, shardID)
	}
}

func estimateBtcFee(inCount, outCount int, feeRate int64, addChange bool) int64 {
	if feeRate < int64(mempool.DefaultMinRelayTxFee) {
		feeRate = int64(mempool.DefaultMinRelayTxFee)
	}

	if addChange {
		inCount += 1
		outCount += 1
	}

	// Version 4 bytes + LockTime 4 bytes + Serialized variant size for the
	// number of transaction inputs and outputs.
	baseSize := 8 +
		wire.VarIntSerializeSize(uint64(inCount)) +
		wire.VarIntSerializeSize(uint64(outCount)) +
		TxInEstWeight*inCount +
		TxOutEstWeight*outCount

	size := int64(baseSize * blockchain.WitnessScaleFactor)
	return feeRate * size // (Satoshi/bytes) * Byte = satoshi
}
