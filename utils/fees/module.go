package fees

import (
	"math"
	"time"

	"gitlab.com/peace-wallet/back-end/proxy-back/logger"
	"gitlab.com/peace-wallet/back-end/proxy-back/utils/fees/models"
)

var actualFees models.GeneralFeesResponse

func GetActualFees() models.GeneralFeesResponse {
	return actualFees
}

func LoadActualFees() error {
	h := NewRequester()

	feeDataRecordExists, err := checkIfFeeDataExists()
	if err != nil {
		logger.Log.Error().Err(err).Msg("cannot check if fee data is written to database")
		return err
	}

	if !feeDataRecordExists {
		return updateActualFees(h, true)
	}

	lastUpdated, err := feeDataLastUpdated()
	if err != nil {
		logger.Log.Error().Err(err).Msg("cannot check when fee data was last updated in database")
		return err
	}

	if time.Since(lastUpdated) < (24 * time.Hour) {
		actualFees, err = getFeeData()
		if err != nil {
			logger.Log.Error().Err(err).Msg("cannot fetch fee data from apis")
			return err
		}
		return nil
	}

	return updateActualFees(h, false)
}

func updateActualFees(r *Requester, createRecord bool) error {
	fees, err := r.fetchNewFees()
	if err != nil {
		return err
	}

	actualFees = calculateAdvisedFee(fees)
	if createRecord {
		return insertFeeData(actualFees)
	}

	return updateFeeData(actualFees)
}

func calculateAdvisedFee(fees []models.GeneralFeesResponse) models.GeneralFeesResponse {
	var slow, moderate, fast float64

	for _, fee := range fees {
		fast += float64(fee.Fast) / float64(len(fees))
		moderate += float64(fee.Moderate) / float64(len(fees))
		slow += float64(fee.Slow) / float64(len(fees))
	}

	return models.GeneralFeesResponse{
		Slow:     int(math.Round(slow)),
		Moderate: int(math.Round(moderate)),
		Fast:     int(math.Round(fast)),
	}
}
