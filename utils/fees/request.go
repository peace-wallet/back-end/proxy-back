package fees

import (
	"io/ioutil"
	"net/http"
)

func doRequest(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	return ioutil.ReadAll(resp.Body)
}
