package fees

import (
	"encoding/json"
	"errors"
	"math"

	"gitlab.com/peace-wallet/back-end/proxy-back/logger"
	"gitlab.com/peace-wallet/back-end/proxy-back/utils/fees/models"
)

type Requester struct {
	requestFuns map[string]func() (models.GeneralFeesResponse, error)
}

func NewRequester() *Requester {
	funs := map[string]func() (models.GeneralFeesResponse, error){
		"bitcoinerLive":   requestFeesBitcoinerLive,
		"blockchainInfo":  requestFeesBlockchainInfo,
		"blockStreamInfo": requestBlockStreamInfoResp,
		"blockCypher":     requestFeesBlockCypher,
	}

	return &Requester{requestFuns: funs}
}

const maxFeeFetchRetries = 10

func (r *Requester) fetchNewFees() ([]models.GeneralFeesResponse, error) {
	var (
		res []models.GeneralFeesResponse
		err error
	)

	for i := 0; i < maxFeeFetchRetries; i++ {
		res, err = r.fetchNewFeesRound()
		if err != nil {
			logger.Log.Warn().Err(err).Msg("Repeating fee fetching in 5 seconds")
			continue
		}
		break
	}

	return res, nil
}

func (r *Requester) fetchNewFeesRound() ([]models.GeneralFeesResponse, error) {
	var res []models.GeneralFeesResponse

	for sourceName, feeReq := range r.requestFuns {
		resp, err := feeReq()
		if err != nil {
			logger.Log.Warn().Err(err).Msg("Failed to fetch fee data from " + sourceName)
			continue
		}
		res = append(res, resp)
	}

	if len(res) == 0 {
		err := errors.New("couldn't fetch any of the fees")
		return nil, err
	}

	return res, nil
}

func requestFeesBitcoinerLive() (models.GeneralFeesResponse, error) {
	body, err := doRequest("https://bitcoiner.live/api/fees/estimates/latest")
	if err != nil {
		return models.GeneralFeesResponse{}, err
	}

	var resp models.BitcoinerLiveResp
	if err := json.Unmarshal(body, &resp); err != nil {
		return models.GeneralFeesResponse{}, err
	}
	// ~ 3 blocks
	fast, ok := resp.Estimates["30"]
	if !ok {
		return models.GeneralFeesResponse{}, models.ErrCannotGetFastFee
	}
	// ~ 6 blocks
	moderate, ok := resp.Estimates["60"]
	if !ok {
		return models.GeneralFeesResponse{}, models.ErrCannotGetModerateFee
	}

	// ~ 12 blocks
	slow, ok := resp.Estimates["120"]
	if !ok {
		return models.GeneralFeesResponse{}, models.ErrCannotGetSlowFee
	}

	return models.GeneralFeesResponse{
		Slow:     int(slow.SatPerVByte),
		Moderate: int(moderate.SatPerVByte),
		Fast:     int(fast.SatPerVByte),
	}, nil
}

func requestFeesBlockchainInfo() (models.GeneralFeesResponse, error) {
	body, err := doRequest("https://api.blockchain.info/mempool/fees")
	if err != nil {
		return models.GeneralFeesResponse{}, err
	}

	var resp models.BlockchainInfoResp
	if err := json.Unmarshal(body, &resp); err != nil {
		return models.GeneralFeesResponse{}, err
	}

	slow := resp.Limits.Min
	moderate := resp.Regular
	fast := resp.Priority

	return models.GeneralFeesResponse{
		Slow:     slow,
		Moderate: moderate,
		Fast:     fast,
	}, nil
}

func requestFeesBlockCypher() (models.GeneralFeesResponse, error) {
	body, err := doRequest("https://api.blockcypher.com/v1/btc/main")
	if err != nil {
		return models.GeneralFeesResponse{}, err
	}

	var resp models.BlockCypherResp
	if err := json.Unmarshal(body, &resp); err != nil {
		return models.GeneralFeesResponse{}, err
	}

	return models.GeneralFeesResponse{
		Slow:     kBToBytes(resp.LowFeePerKB),
		Moderate: kBToBytes(resp.MediumFeePerKB),
		Fast:     kBToBytes(resp.HighFeePerKB),
	}, nil
}

func requestBlockStreamInfoResp() (models.GeneralFeesResponse, error) {
	body, err := doRequest("https://blockstream.info/api/fee-estimates")
	if err != nil {
		return models.GeneralFeesResponse{}, err
	}

	var resp models.BlockStreamInfoResp
	if err := json.Unmarshal(body, &resp); err != nil {
		return models.GeneralFeesResponse{}, err
	}

	slow, ok := resp["12"]
	if !ok {
		return models.GeneralFeesResponse{}, models.ErrCannotGetSlowFee
	}

	moderate, ok := resp["6"]
	if !ok {
		return models.GeneralFeesResponse{}, models.ErrCannotGetModerateFee
	}

	fast, ok := resp["3"]
	if !ok {
		return models.GeneralFeesResponse{}, models.ErrCannotGetFastFee
	}

	return models.GeneralFeesResponse{
		Slow:     int(math.Round(slow)),
		Moderate: int(math.Round(moderate)),
		Fast:     int(math.Round(fast)),
	}, nil
}

func kBToBytes(kB int) int {
	return int(math.Round(float64(kB) / 1000))
}
