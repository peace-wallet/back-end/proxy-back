package fees

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/peace-wallet/back-end/proxy-back/database"
	"gitlab.com/peace-wallet/back-end/proxy-back/utils/fees/models"
)

func getFeeData() (models.GeneralFeesResponse, error) {
	q := `SELECT slow, moderate, fast FROM fee_rates WHERE id = 1`
	rows, err := database.DB.Query(context.Background(), q)
	if err != nil {
		return models.GeneralFeesResponse{}, err
	}
	defer rows.Close()

	var result models.GeneralFeesResponse
	for rows.Next() {
		err = rows.Scan(&result.Slow, &result.Moderate, &result.Fast)
		if err != nil {
			return models.GeneralFeesResponse{}, err
		}
	}

	return result, nil
}

func insertFeeData(fees models.GeneralFeesResponse) error {
	q := `INSERT INTO fee_rates VALUES (1, %d, %d, %d)`
	_, err := database.DB.Exec(context.Background(), fmt.Sprintf(q, fees.Slow, fees.Moderate, fees.Fast))
	if err != nil {
		return err
	}

	return nil
}

func updateFeeData(fees models.GeneralFeesResponse) error {
	q := `UPDATE fee_rates SET slow = %d, moderate = %d, fast = %d WHERE id = 1`
	_, err := database.DB.Exec(context.Background(), fmt.Sprintf(q, fees.Slow, fees.Moderate, fees.Fast))
	if err != nil {
		return err
	}

	return nil
}

func checkIfFeeDataExists() (bool, error) {
	q := `SELECT * FROM fee_rates`
	rows, err := database.DB.Query(context.Background(), q)
	if err != nil {
		return false, err
	}

	return rows.Next(), nil
}

func feeDataLastUpdated() (time.Time, error) {
	q := `SELECT updated_at FROM fee_rates`
	var lastUpdated time.Time
	rows, err := database.DB.Query(context.Background(), q)
	if err != nil {
		return time.Time{}, err
	}

	for rows.Next() {
		err = rows.Scan(&lastUpdated)
		if err != nil {
			return time.Time{}, err
		}
	}

	return lastUpdated, nil
}
