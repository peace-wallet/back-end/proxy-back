package models

import "errors"

var (
	ErrCannotGetSlowFee     = errors.New("cannot find slow fee estimate")
	ErrCannotGetModerateFee = errors.New("cannot find moderate fee estimate")
	ErrCannotGetFastFee     = errors.New("cannot find fast fee estimate")
)
