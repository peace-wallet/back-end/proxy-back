package models

type GeneralFeesResponse struct {
	Slow     int
	Moderate int
	Fast     int
}

type BitcoinerLiveResp struct {
	Estimates map[string]BitcoinerLiveEstimate `json:"estimates"`
}

type BitcoinerLiveEstimate struct {
	SatPerVByte float64 `json:"sat_per_vbyte"`
}

type BitGoResp struct {
	FeeByBlockTarget map[string]int `json:"feeByBlockTarget"`
}

type BlockchainInfoResp struct {
	Limits struct {
		Min int `json:"min"`
		Max int `json:"max"`
	} `json:"limits"`
	Regular  int `json:"regular"`
	Priority int `json:"priority"`
}

type BlockCypherResp struct {
	HighFeePerKB   int `json:"high_fee_per_kb"`
	MediumFeePerKB int `json:"medium_fee_per_kb"`
	LowFeePerKB    int `json:"low_fee_per_kb"`
}

type BlockStreamInfoResp map[string]float64
