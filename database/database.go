package database

import (
	"context"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/peace-wallet/back-end/proxy-back/config"
)

var (
	// DB is a common connections pool for all components.
	// pgxpool is a concurrency-safe pool implementation,
	// so it is safe to use it as singleton once initialised.
	DB *pgxpool.Pool
)

func InitConnectionsPool() (err error) {
	DB, err = pgxpool.Connect(context.Background(), config.Config.Database.PSQLConnectionCredentials())
	return
}
