/*
 * Copyright (c) 2022 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package database

import (
	"database/sql"
	"embed"

	"github.com/pkg/errors"
	migrate "github.com/rubenv/sql-migrate"
)

const (
	// MigrateUp causes migrations to be run in the "up" direction.
	MigrateUp = "up"
	// MigrateDown causes migrations to be run in the "down" direction.
	MigrateDown = "down"
)

//go:embed schema/*
var schema embed.FS

func Migrate(db *sql.DB, direction string) (int, error) {
	return MigrateSet(db, direction, Migrations{
		EnablePatchMode: true,
		Table:           "_migrations",
		Assets:          NewAssetSource(&schema, "schema"),
	})
}

func NewAssetSource(fs *embed.FS, rootDirectory string) migrate.MigrationSource {
	return &migrate.AssetMigrationSource{
		Asset: fs.ReadFile,
		AssetDir: func() func(string) ([]string, error) {
			return func(path string) ([]string, error) {
				dirEntry, err := fs.ReadDir(path)
				if err != nil {
					return nil, err
				}
				entries := make([]string, 0)
				for _, e := range dirEntry {
					entries = append(entries, e.Name())
				}

				return entries, nil
			}
		}(),
		Dir: rootDirectory,
	}
}

func MigrateSet(db *sql.DB, direction string, set ...Migrations) (int, error) {
	switch direction {
	case MigrateUp:
		return MigrateSetUp(db, set...)
	case MigrateDown:
		return MigrateSetDown(db, set...)

	}
	return 0, nil
}

func MigrateSetUp(db *sql.DB, set ...Migrations) (int, error) {
	var total int

	applier, err := NewExecutor(db)
	if err != nil {
		return 0, err
	}

	for _, migrations := range set {
		applier = applier.SetMigrations(migrations)

		count, err := applier.Migrate(MigrateUp)
		if err != nil {
			return 0, err
		}
		total += count
	}

	return total, nil
}

func MigrateSetDown(db *sql.DB, set ...Migrations) (int, error) {
	var total int

	applier, err := NewExecutor(db)
	if err != nil {
		return 0, err
	}

	for i := len(set) - 1; i >= 0; i-- {
		applier = applier.SetMigrations(set[i])

		count, err := applier.Migrate(MigrateDown)
		if err != nil {
			return 0, err
		}
		total += count
	}

	return total, nil
}

type Migrations struct {
	Table           string
	Schema          string
	EnablePatchMode bool
	Assets          migrate.MigrationSource
}

type Executor struct {
	Migrations

	db *sql.DB
}

func NewExecutor(db *sql.DB) (*Executor, error) {
	return &Executor{db: db}, nil
}

func (executor *Executor) SetMigrations(set Migrations) *Executor {
	executor.Migrations = set
	return executor
}

// Migrate connects to the database and applies migrations.
func (executor *Executor) Migrate(dir string) (int, error) {
	if executor.Assets == nil {
		return 0, errors.New("migrations isn't set")
	}

	applier := migrate.MigrationSet{
		TableName:     executor.Table,
		SchemaName:    executor.Schema,
		IgnoreUnknown: false,
	}

	return applier.Exec(executor.db, "postgres", executor.Assets, executor.directions()[dir])
}

func (executor *Executor) directions() map[string]migrate.MigrationDirection {
	return map[string]migrate.MigrationDirection{
		MigrateUp:   migrate.Up,
		MigrateDown: migrate.Down,
	}
}
