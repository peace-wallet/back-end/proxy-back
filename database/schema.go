package database

import (
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/stdlib"
	"gitlab.com/peace-wallet/back-end/proxy-back/config"
)

// EnsureSchema executes provided schema definition SQL file.
// By default, it ensures all required tables / indexes / constraints / triggers are present.
// In case if some table (or index, or ...) is absent - it would be recreated.
// Does not replaces tables in case if them are present (to not to drop data occasionally).
func EnsureSchema() (err error) {
	pgxCfg, err := pgx.ParseConfig(config.Config.Database.PSQLConnectionCredentials())
	if err != nil {
		return err
	}

	pgxCfg.PreferSimpleProtocol = true

	sqldb := stdlib.OpenDB(*pgxCfg)
	_, err = Migrate(sqldb, MigrateUp)
	return err
}
