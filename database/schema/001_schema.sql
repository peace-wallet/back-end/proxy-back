-- -*- mode: sql; sql-product: postgres; -*-
-- +migrate Up

create table if not exists subscriptions
(
    id                                 bigserial primary key,
    constraint id_gte_zero check (id >= 0),

    uuid                               uuid     not null,
    constraint unique_uuid unique (uuid),

    ead                                text     not null,
    token                              text     not null,
    source_shard_id                    bigint   not null,
    constraint source_shard_id_gte_zero
        check (source_shard_id >= 0),

    destination_shard_id               bigint   not null,
    constraint destination_shard_id_gte_zero
        check (destination_shard_id >= 0),

    source_shard_max_block_height      bigint   not null,
    constraint source_shard_max_block_height_gte_zero
        check (source_shard_max_block_height >= 0),

    destination_shard_max_block_height bigint   not null,
    constraint destination_shard_max_block_height_gte_zero
        check (destination_shard_max_block_height >= 0),

    status                             smallint not null default 0,
    constraint state_ge_zero check (status >= 0)
);

create table if not exists fee_rates
(
    id         integer primary key,
    slow       smallint                    not null,
    moderate   smallint                    not null,
    fast       smallint                    not null,
    created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now()
);

-- +migrate Down
